# drEEM toolbox for MATLAB
MATLAB toolbox aiding the multiway decomposition of fluorescence EEMs into underlying fluorescence components.

## Installation
- Download the latest version: http://dreem.openfluor.org/ -> Download latest
- Copy drEEM folder into a local directory (e.g. .../user/documents/MATLAB)
- Change the current MATLAB directory to the folder containing `dreeminstall`
- Run `dreeminstall`
- To independantly test the installation, you can run `lookforconflicts('drEEM')` and `lookforconflicts('nway')` at any time

## Usage
- Type `help drEEM` for for an overview of functions.

## Further reading (peer-reviewed publications)
- [drEEM toolbox](https://doi.org/10.1039/c3ay41160e)
- [N-way toolbox](https://doi.org/10.1016/S0169-7439(00)00071-X)
- [OpenFluor](https://doi.org/10.1039/C3AY41935E)

## Further watching
- [Getting started with drEEM](http://bit.ly/2wXCdKW)
- [smootheem tutorial](http://bit.ly/2IytZOI)
- [splitds tutorial](http://bit.ly/2ICxXlo)
- [OpenFluor webinar](https://www.youtube.com/watch?v=oKC5yt4v0BA)
- [Lecture series on multiway analysis](https://www.youtube.com/watch?v=_gIb6PzBEc4&list=PL4L59zaizb3E-Pgp-f90iKHdQQi15JJoL)

## Questions, comments , suggestions?
- Visit the [drEEM user support](https://www.researchgate.net/project/drEEM-user-support) to see if your questions haven't been answered before.
- Email us at murphyk@chalmers.se, urbw@aqua.dtu.dk
- Message [Kathleen Murphy](https://www.researchgate.net/profile/Kathleen_Murphy15) or [Urban Wünsch](https://www.researchgate.net/profile/Urban_Wuensch) on ResearchGate

## Version history
- 0.6.0 : August 2020 : Sixth release (Urban J. Wuensch & Kathleen Murphy)
- 0.5.0 : April  2019 : Fifth release (Urban J. Wuensch & Kathleen Murphy)
- 0.4.0 : June   2017 : Fourth release (Kathleen Murphy)
- 0.2.0 : June   2014 : Second release (Kathleen Murphy)
- 0.1.0 : Sept   2013 : Initial release (Kathleen Murphy)