## drEEM tutorials
This folder contains data and .m-files that belong to tutorials given in the
MATLAB documentation. Type 'doc dreem' to access drEEM's documentation and 
navigate to the tutorial section.

The IFE demo is the sole remaining demo that has not been moved to the MATLAB
documentation. This particular demo is located in a subfolder of this one ("IFE demo").