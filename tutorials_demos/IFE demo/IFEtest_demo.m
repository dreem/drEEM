%IFEtest_demo
%Loads a corrected sample from the NORD_B dataset for testing effects of
%IFE correction and noise against theoretical values
%Loaded variables are:
%Xt  corrected EEM from the NORDB dataset
%Xb  uncorrected blank EEM from the NORDB dataset
%Ex  excitation wavelengths
%Em  emission wavelengths
%A   absorbance scan corresponding to Xt. First row lists wavelengths.
%DemoInputs: a data structure listing error and noise levels as
%  were originally specified in this mfile. It contains:
%    Noise_F: Measured amplitude of noise for HJY FluoroLog FL3-22: 3.5e-04
%    Noise_A: Assumed amplitude of noise in Abs spectral blank: 1e-2
%    DilErr:  Measurement error (%) for 2x dilution (gravimetric method): 1e-2
%    Abs_bias: bias level(%) and onset wavelength: [wavelength, bias]:[325 -0.03]
%    Abs_bias_type: {'absolute','proportional','none',[]}: proportional
%
% Notice:
% This mfile is part of the drEEM toolbox. Please cite the toolbox
% as follows:
%
% Murphy K.R., Stedmon C.A., Graeber D. and R. Bro, Fluorescence
%     spectroscopy and multi-way techniques. PARAFAC, Anal. Methods, 
%     5, 6557-6566, 2013. DOI:10.1039/c3ay41160e. 
%
% For further information about IFEtest see:
%
% D.N. Kothawala, K.R. Murphy, C.A. Stedmon, G.A.Weyhenmeyer, and L.J. Tranvik, 
% Inner filter correction of dissolved organic matter fluorescence. 
% Limnol. Oceanogr.: Methods 11, 2013, 
% 
% IFEtest_demo: Copyright (C) 2013 Kathleen R. Murphy
% $ Version 0.1.1 $ November 2013 
%
% Copyright (C) 2013 KR Murphy,
% Water Research Centre
% The University of New South Wales
% Department of Civil and Environmental Engineering
% Sydney 2052, Australia
% krm@unsw.edu.au
%%%%%%%%%%%%%%%%

echo off

global RunningTheDemo
RunningTheDemo=true;

%Load the NORD_B dataset, start with the CDA-corrected EEM (most
%concentrated) and its corresponding Abs spectrum and matrix of Atotal
%A CDA-corrected, 2x diluted NORD_B sample is chosen as the "undiluted
%theoretical EEM" (sample 28, which has A(200)=2.37 and A(250)=1.42);


disp(' ')
disp(' ')
disp('----------------------IFEtest DEMO---------------------------------')
disp(' ')
disp('This script examines the effect of instrument variability on  ')
disp('correcting for inner filter effects (IFE) by absorbance and controlled ')
disp('dilution approaches. For further information see:')
disp(' ')
disp('D.N. Kothawala, K.R. Murphy, C.A. Stedmon, G.A.Weyhenmeyer, and L.J. Tranvik,')
disp(' Inner filter correction of dissolved organic matter fluorescence.')
disp(' Limnol. Oceanogr.: Methods 11, 2013.')
disp('-------------------------------------------------------------------')
disp(' ')
disp('Choose a dataset for IFE correction:')
disp(' 1) Peak C from NORD_B dataset')
disp(' 2) Peak T from NORD_B dataset')
disp(' 3) Peak A from NORD_B dataset')
disp(' ')
choice = input(' Choose 1 or 2 or 3: ');
disp(' ')
disp(' ')

if choice==1
    echo on
    ExEmPair=[350 452];
    ExEmPeak=char('C');
    echo off
elseif choice==2
    echo on
    ExEmPair=[300 352];
    ExEmPeak=char('T');
    echo off
elseif choice==3
    echo on
    ExEmPair=[250 452];
    ExEmPeak=char('A');
    echo off
else
    error('Must select a number between 1-3. Please restart demo');
end

disp(' ')
disp(' ')

try
    fprintf('Error levels to be used in the demo: \n')
    disp(DemoInputs);
catch
    RestoreDemoInputs
    DemoInputs
end
echo on
pause

% This script applies the ACA (Absorbance correction approach) 
% and CDA (controlled dilution approach) methods for performing 
% inner filter error (IFE) correction of fluorescence data. 
%
% By assuming various levels of instrumental and measurement
% error, the effects of error on IFE corrections can be examined.
%
% The model does not account for increasing deviations between theoretical
% and actual inner filter effects occurring at higher absorbances, and
% is most useful for Atotal(Aex + Aem) < 1.5.
%
% We start by loading the data
% As a theoretical EEM we are using the average of two corrected EEMs
%
% 1. CDA-corrected EEM of sample 28
% 2. ACA-corrected EEM of sample 28
%
% We will also use the Abs spectrum for this sample
% and the MilliQ blank for the NORD_B dataset
% 
% (When the screen indicates pause mode, hit any key to continue....)

pause
%-------------------------------------------------
load 'NORDB_demodata.mat';

%Choose a name for this test
TestName='MY_IFE_TEST';

  pause
%-------------------------------------------------

%We will assume maximum absolute levels of random noise (in the same units as the data)
%for the fluorometer (Raman Units) and the spectrophotometer (Absorbance its unitless). 
%We will also assume a level of measurement error when diluting samples to 2x
%original strength for CDA.

echo off
disp('   ')
disp('   ')

Noise_F=DemoInputs.Noise_F
Noise_A=DemoInputs.Noise_A
DilErr=DemoInputs.DilErr

disp('   ')
echo on
pause


%-------------------------------------------------
% We will now add in spectrophometer bias, i.e. overestimation or underestimation
% of absorbance above a specified onset wavelength. This error can be 'proportional'
% e.g. a 5% positive bias, or 'absolute', e.g. a bias of 0.05 absorbance units.
%
% We will also specify the lower limit of wavelengths affected by bias. For
% example:
%  [350 -0.05] with Abs_bias_type='proportional' indicates a 5% negative bias above 350 nm
%  [200 0.01] with Abs_bias_type='proportional' indicates a 0.01 RU positive bias above 200 nm
%

echo off
disp('   ')
disp('   ')

Abs_bias=DemoInputs.Abs_bias %[wavelength, bias]
Abs_bias_type=DemoInputs.Abs_bias_type %possible inputs are {'absolute','proportional','none',[]}

disp('   ')
echo on
pause

%-------------------------------------------------
%Choose whether or not to make and save figures and save data {true/false} 
%saved figures will replace previously saved figures in same location
%saved data will replace previously saved data in same location
% 

DoPlots=true; 
SaveData=true; 

pause
%-------------------------------------------------
%Now lets plot our 'theoretical' data
echo off
figure, 
subplot(2,1,1),plot(A(1,:),A(2,:)),title('theoretical Absorbance')
subplot(2,2,3),contourf(Ex,Em,squeeze(Xt)),colorbar,title('theoretical EEM');
xlabel('Ex'),ylabel('Em'),
subplot(2,2,4),contourf(Ex,Em,squeeze(Xb)),colorbar,title('theoretical blank');
xlabel('Ex'),ylabel('Em'),
%print Absorbance data to screen
fprintf('\n\n')
disp('Display some Absorbance readings for this sample');
disp([A(:,A(1,:)==200) A(:,A(1,:)==250) A(:,A(1,:)==300) A(:,A(1,:)==350) A(:,A(1,:)==400) A(:,A(1,:)==445)]);
fprintf('\n\n')
echo on

pause
%-------------------------------------------------
% Next we will run the IFE test by performing theoretical dilutions 
% on the EEMs and Abs spectra, and adding theoretical IFEs plus our chosen
% noise/blanks/error. Then we will correct the noisy data by ACA and CDA 
% and compare the resulting dilution series to the theoretical dilution series. 
% Finally, we will examine the effect of instrument noise, measurement error and 
% unsubtracted blanks on the two correction methods

echo off
disp('   ')
disp('   ')
echo on

dataout=IFEtest(Ex,Em,Xt,Xb,A,ExEmPair,Noise_F,Noise_A,Abs_bias,Abs_bias_type,DilErr,DoPlots,SaveData,TestName);
echo off
