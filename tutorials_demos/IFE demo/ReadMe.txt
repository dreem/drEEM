Instructions to use IFEtest_demo

Extract IFEdemo and place it on your MATLAB path, or change the current directory to this folder, e.g.
                        > cd 'C:/mydirectories/IFEdemo

At the cursor (>) type: IFEtest_demo

The default inputs to the demo are found here: 
						>DemoInputs

Assumed error levels can be changed by directly editing DemoInputs. For example:
						> DemoInputs.Noise_A=0.005
						> DemoInputs.Abs_bias:[200 0]
						etc.

To restore the original values of DemoInputs type: RestoreDemoInputs 

DISCLAIMER
This toolbox has been tested in MATLAB Versions: 7.14 (R2012a) and R2014a
and requires access to the Statistics and Symbolic Math toolboxes.
Earlier versions of MATLAB may not be able to run this code.
To obtain a your MATLAB version and a list of installed toolboxes type: >ver


REFERENCE
D.N. Kothawala, K.R. Murphy, C.A. Stedmon, G.A.Weyhenmeyer, and L.J. Tranvik, Inner filter correction of 
dissolved organic matter fluorescence. Limnol. Oceanogr.: Methods 11, 2013, 
