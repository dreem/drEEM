function dataout=IFEtest(Ex,Em,Xt,Xb,A,ExEmPair,Noise_F,Noise_A,Abs_bias,Abs_bias_type,DilErr,DoPlots,SaveData,TestName)
% Run the IFE test by performing theoretical dilutions on a 
% theoretical EEM + Abs spectra, then adding theoretical IFEs plus our chosen
% noise/blanks/error. Then correct the noisy data by ACA and CDA 
% and compare the resulting dilution series to the theoretical dilution series. 
% Input Variables:
%   Ex  excitation wavelengths
%   Em  emission wavelengths
%   Xt  corrected EEM from the NORDB dataset
%   Xb  uncorrected blank EEM from the NORDB dataset
%   A   absorbance scan corresponding to Xt. First row lists wavelengths.
%   ExEmPair:   excitation and emission wavelength for plotting;
%   Noise_F:    Amplitude of noise in fluorescence MilliQ EEM
%   Noise_A:    Amplitude of noise in Abs Spectral blank 
%   Abs_bias:   bias level and onset wavelength: [wavelength, bias]
%   Abs_bias_type: {'absolute','proportional','none',[]}
%   DilErr:     Assumed measurement error when diluting samples to 2x
%   DoPlots:    name assigned to ExEmPair;
% Output Variables:
%   dataout = a table of regression statistics
% This mfile requires access to MATLAB and the following two toolboxes
% 1. statistics_toolbox (regstats, linhyptest and robustfit functions)
% 2. symbolic_toolbox (disp, several matrix functions)
%
% Notice:
% This mfile is part of the drEEM toolbox. Please cite the toolbox
% as follows:
%
% Murphy K.R., Stedmon C.A., Graeber D. and R. Bro, Fluorescence
%     spectroscopy and multi-way techniques. PARAFAC, Anal. Methods, 
%     5, 6557-6566, 2013. DOI:10.1039/c3ay41160e. 
%
% For further information about IFEtest see:
%
% Kothawala et al, Inner filter correction of dissolved organic matter 
%                  Limnol. Oceanogr.: Methods 11, 2013, 
% 
% IFEtest: Copyright (C) 2013 Kathleen R. Murphy
% $ Version 0.1.1 $ November 2013 
%
% Copyright (C) 2013 KR Murphy,
% Water Research Centre
% The University of New South Wales
% Department of Civil and Environmental Engineering
% Sydney 2052, Australia
% krm@unsw.edu.au
%%%%%%%%%%%%%%%%

global RunningTheDemo
SaveFigures=true;

%Check input variables and assign defaults
if nargin>14;error('too many input arguments'),end
if nargin<14;TestName='Test';end
if nargin<13;SaveData=false;end
if nargin<12;DoPlots=false;SaveFigures=false;end
if nargin<11;DilErr=0;end
if nargin<10;Abs_bias_type='none';end
if nargin<9;Abs_bias=[0 0];end
if nargin<8;Noise_A=0;end
if nargin<7;Noise_F=0;end
if nargin<6;error('too few input arguments'),end
if isempty(Xb);Xb=zeros(size(Xt(1,:,:)));end
if isempty(Noise_A);Noise_A=0;end
if isempty(Abs_bias);Abs_bias=[0 0];end
if isempty(Noise_F);Noise_F=0;end
if isempty(DilErr);DilErr=0;end
if isempty(TestName);TestName='Test';end
if isempty(Abs_bias_type);Abs_bias_type='none';end

if DoPlots==false;SaveFigures=false;end

%check that the necessary MATLAB toolboxes and mfiles are installed
featureStr = {'statistics_toolbox'; ... 
              'symbolic_toolbox'};
checktoolboxes(featureStr);
dorobustfit=true;doODEfit=true;
%search for stats toolbox functions
if isempty(which('robustfit'));
    dorobustfit=false;
    warning('robustfit function not found (check MATLAB:STATS toolbox is on path)');
end
if isempty(which('regstats'));
    doODEfit=false;
    warning('regstats function not found (check MATLAB:STATS toolbox is on path)');
end
if and(dorobustfit==false,doODEfit==false)
    error('Robustfit and Regstats functions not found on the MATLAB path. Aborting.....')
end

%Display Test information
TestInfo=[TestName ' ' num2str(ExEmPair) '-EEM noise: ' num2str(Noise_F) ', ABS noise: ' num2str(Noise_A)  ...
    ', bias: ' num2str(Abs_bias(2)) '@' num2str(Abs_bias(1)) 'nm ' Abs_bias_type ', Dil Err: ' num2str(DilErr)];
fprintf('\n')
disp(TestInfo)
fprintf('\n')

%check for too many open figures
if RunningTheDemo
 disp('pause')
 pause
 disp('-------------------------------------------------')
end
nfigs = length(get(0,'Children'));
if nfigs>15
    warning('WarnTest:ManyOpenFigures',['There are currently ' num2str(nfigs) ...
        ' open figure windows. Consider closing some windows to free up memory'])
    fprintf('hit any key to continue, or Control-C to cancel. \n\n');
    pause
end

%% Intialise
%Create a dataset of theoretical EEMs
%choose the EEM dilution factor for CDA (recommended = 2)
%construct dilutions so that samples pairs(2):end are 2x dilutions of samples 1:end-pairs(1)
%DilutionFactors=fliplr(2.^(-6:0.2:0));  %the dilutions to be applied to the EEM
%pairs=[5 6];                            %corresponds to 2x dilution factor when applying CDA, 26 samples in the dilution series
DilutionFactors=fliplr(2.^(-6:0.5:0));   %the dilutions to be applied to the EEM
pairs=[2 3];                             %corresponds to 2x dilution factor when applying CDA, 11 samples in the dilution series
div=DilutionFactors(1:end-pairs(1))./DilutionFactors(pairs(2):end);
No_Of_CDA_pairs=length(div);
NoSamples=length(DilutionFactors);
if abs(max(div-2*ones(1,length(div))))>0.1;
    warning('Some dilution factors proposed for CDA may differ from recommended (2x) by 5% or more')
    disp(div)
end

%Dataset created from A
Abtwaves= A(1,:); %200:1:600 nm, ABS
Abtdata= A(2,:); %its theoretical absorbance spectrum
Abs=repmat(Abtdata,[NoSamples,1]);

%Dataset created from Xt
Xt(~isfinite(Xt))=NaN;
Xt=repmat(Xt,[NoSamples,1,1]);

%Get theoretical Atot matrix for this sample
AtotMat=NaN*ones(1,length(Em),length(Ex));
for j=1:length(Em)
    for k=1:length(Ex)
        AtotMat(1,j,k)=Abtdata(Abtwaves==Em(j))+Abtdata(Abtwaves==Ex(k));
    end
end
At=repmat(AtotMat,[NoSamples,1,1]); %matrix of Atot calculated from Abs spectrum
%size(Abs),size(At),size(Xt)

%apply dilutions
IFE=NaN*ones(size(Xt));
for i=1:length(DilutionFactors)
    Abs(i,:)=DilutionFactors(i)*Abs(i,:);   %theoretical diluted Abs spectra
    At(i,:,:)=DilutionFactors(i)*At(i,:,:); %theoretical diluted Atot
    Xt(i,:,:)=DilutionFactors(i)*Xt(i,:,:); %theoretical diluted EEMs
    IFE(i,:,:)=10.^(1/2*squeeze(At(i,:,:))); %theoretical IFE effect
end
Xu=Xt./IFE; %Theoretical dilution series with inner filter effects

%plots of Xt with/without IFEs
if DoPlots
    if RunningTheDemo
        disp('Figure: Compare theoretical EEM with/without inner filter effects')
        pause
    end
    h3=figure;
    set(gcf,'name','Visualise Inner Filter Effects')
    subplot(2,2,1), contourf(Ex,Em,squeeze(Xt(1,:,:))),colorbar,title('Theoretical sample no IFEs')
    subplot(2,2,2),contourf(Ex,Em,squeeze(Xu(1,:,:))),colorbar,title('Theoretical sample with IFEs')
    subplot(2,2,3),contourf(Ex,Em,squeeze(IFE(1,:,:))),colorbar,title('IFE effect')
    subplot(2,2,4), plot(Abs(1,:,:)),colorbar,title('Theoretical Abs spectrum')
    if RunningTheDemo
        disp('pause')
        disp('-------------------------------------------------')
        pause
    end
end

%% ABS spectra, add noise and bias to measured spectra
%Add random noise to the diluted Abs spectra
%this noise will affect ACA correction
AbsCases=char('no ABS noise','random ABS noise','ABS noise+bias');
Abs_Case_data(1,:,:)=Abs;                                         %case 1: no error
Abs_Case_data(2,:,:)=Abs+Noise_A*0.5*rand_neg1to1(Abs);           %case 2: random noise

%add a bias to the Abs spectra at landa>Abs_bias(1)
%e.g. Abs_bias=[350 0.05] adds a positive bias (overestimate) at A>350 nm
bias=zeros(1,length(Abtwaves));
bias(Abtwaves>=Abs_bias(1))=Abs_bias(2);
if isequal(Abs_bias_type,'proportional')
    A_bias=Abs.*repmat(bias,[NoSamples 1]);
else                                                              %absolute bias, or no bias
    A_bias=repmat(bias,[NoSamples 1]);
end
Abs_Case_data(3,:,:)=squeeze(Abs_Case_data(2,:,:))+A_bias;        %case 3: random noise plus bias
Abs_Case_data(Abs_Case_data<0)=0;                                 %negative Abs set to zero

%% Apply noise to EEMs
FluorCases=char('EEM no noise','EEM wave-indep. noise','EEM wave-dep. noise');

%create curve with maximum at 250 nm, reaches half amplitude after 20 nm
f_x=Ex-Ex(1); f_y=20./(f_x+20);                                                      %curve
NoiseDecay= permute(repmat(repmat(f_y,[length(Em) 1]),[1 1 NoSamples]),[3 1 2]);     %3D curve
%figure, contourf(squeeze(NoiseDecay(1,:,:))),colorbar

Xb_Case_data(1,:,:,:)=zeros(size(Xu));                                               %case 1: no noise
Xb_Case_data(2,:,:,:)=Noise_F*rand_neg1to1(Xu);                                      %case 2: random constant noise (no wavelength dependency)
Xb_Case_data(3,:,:,:)=Noise_F*NoiseDecay.*rand_neg1to1(Xu);                          %case 3: wavelength-dependent noise;

%blank
BlankCases=char('no EEM blank','EEM blank'); 
EEMblk(1,:,:,:)=zeros(size(Xu));              %3D matrix of zeros
EEMblk(2,:,:,:)=repmat(Xb,[NoSamples,1,1]);   %3D matrix of Xb   

%% Dilution error, affecting estimation of p in CDA
%Dilution error is equal to zero (off) or DilErr (on)
DilErrCases=char('no dilution error','dilution error');
p_err=[0;DilErr];

%% Regressions and figures for selected ExEmPeak
iiP=1:No_Of_CDA_pairs;
cases_to_plot=1:7;
NaNmat=NaN*ones([2,max(cases_to_plot),1]);
F_mat=NaNmat;p_mat=NaNmat;DF=NaNmat;Yint=NaNmat;M=NaNmat;Rsq_=NaNmat;StdErr_y=NaNmat;StdErr_m=NaNmat;
dataout=repmat(NaN*ones(2,16,1),[max(cases_to_plot),1]);
if RunningTheDemo
    disp('Figures: Theoretical dilution series vs. ABA- and CDA-corrected series')
    disp('pause')
    pause
end
if DoPlots,h3=figure;h4=figure;h5=figure;h6=figure;h7=figure;end
for b=cases_to_plot %b=1
    %Cases to Plot
    switch b %produce scenarios representing [ fl. noise, abs. noise, dil. err, fl. blank]
        case 1 %no noise or bias
            scenario=[1 1 1 1]; %yrange=[-0.01 0.01];
        case 2 %fluorometer noise
            scenario=[2 1 1 1];%yrange=[-0.05 0.05];
        case 3 %absorbance noise+bias (proportional)
            scenario=[1 3 1 1];%yrange=[-0.25 0.25];
        case 4 %dilution error
            scenario=[1 1 2 1];%yrange=[-0.25 0.25];
        case 5 %fluorometer blank
            scenario=[1 1 1 2];%yrange=[];
        case 6 %fluorometer noise,absorbance noise+bias (proportional) + dilution error
            scenario=[2 3 2 1];%yrange=[-0.25 0.25];
        case 7 %fluorometer blank+noise, abs noise+bias, dilution error
            scenario=[2 3 2 2];%yrange=[];
    end
    
    titl=[deblank(FluorCases(scenario(1),:)) ',' deblank(BlankCases(scenario(4),:)) ',' deblank(AbsCases(scenario(2),:)) ',' deblank(DilErrCases(scenario(3),:))];
    disp(['Case ' num2str(b) ': ' titl])
    
    %Choose type of fluorescence noise/blank corresponding to case=b
    X_noise=squeeze(Xb_Case_data(scenario(1),:,:,:));
    Abs_b=squeeze(Abs_Case_data(scenario(2),:,:));
    p_adj=p_err(scenario(3)).*rand_neg1to1(div)'; 
    fl_blank=squeeze(EEMblk(scenario(4),:,:,:));
    
    %Apply theoretical IFEs to get the true uncorrected EEM
    Xm=(Xt+fl_blank); %add the blank, if present
    Xm=Xm./IFE;       %the theoretical measured EEM, accounting for the blank and IFEs
    Xm=Xm+X_noise;    %the actual measured EEM, accounting for noise
    Xm(Xm<0)=0;       %negative fluorescence set to zero
    
    %Correct the EEMs with CDA
    L=NaN*ones(size(Xm));H=NaN*ones(size(Xm));S=NaN*ones(size(Xu));
    X1mat=Xm(1:end-pairs(1),:,:);
    X2mat=Xm(pairs(2):end,:,:);
    for i=1:length(div)
        p=div(i)*(1+p_adj(i));
        X1=squeeze(X1mat(i,:,:));X1=[[NaN Ex];[Em X1]]; %the undiluted EEM
        X2=squeeze(X2mat(i,:,:));X2=[[NaN Ex];[Em X2]]; %the diluted EEM
        [Li,Hi,Si] = CDAife(X1,X2,p);
        L(i,:,:)=Li;
        H(i,:,:)=Hi;
        S(i,:,:)=Si;
    end
    %disp(S(i,j,k)); %CDA sensitivity for ExEmPair

    %Correct the EEMs by ACA, using measured Abs with bias and noise
    absin=[Abtwaves;Abs_b];
    [K,IFE_m] = ABAife(Ex,Em,absin,Xm); %#ok<NASGU>
    
    %compare measured (IFE_m) and theoretical (IFE) inner filter effects
    %[IFE_m(:,Em==452,Ex==350) IFE(:,Em==452,Ex==350)];
    %[IFE_m(:,Em==348,Ex==300) IFE(:,Em==348,Ex==300)]
    
    %Calculate Regressions and plot selected wavelength pair
    j=find(Em==ExEmPair(2));
    k=find(Ex==ExEmPair(1));
    if isempty(j), disp(Em);error('ExEmPair contains emission wavelength not found in Em');end    
    if isempty(k), disp(Ex);error('ExEmPair contains excitation wavelength not found in Ex');end    
    
    %Robust Regression
    x_in_log=log(At(iiP,j,k));
    y_ins=[K(iiP,j,k) L(iiP,j,k)];
    y_ins(~isfinite(y_ins))=NaN; %only finite numbers allowed
    y_ins(y_ins<=0)=NaN; %only positive numbers allowed
    y_in_logs=log(y_ins);
    for n=1:2
        y_in_log=y_in_logs(:,n);
        try
            %figure, plot(x_in_log(iiR),y_in_log(iiR),'ko')
            %regression on the normalized data. The slope should be 1, test.
            if and(doODEfit,dorobustfit) %do robust linear regression
                [brob,s] = robustfit(x_in_log,y_in_log); %robust regression of ABS, leave out high Atot data
                s11 = regstats(x_in_log,y_in_log);       %ODE regression of ABS, leave out high Atot data
                [p_RR,F_RR] = linhyptest(brob, s11.covb, 1, [0 1], s11.tstat.dfe); %performs an F-test that the slope of robust regression is not sign diff to 1.
                yint=brob(1);
                m=brob(2);
                p_=p_RR;
                F_=F_RR;
                sey=s.se(1);
                sem=s.se(2);
                sse = s.dfe * s.robust_s^2;
                phat = brob(1) + brob(2)*x_in_log;
                ssr = norm(phat-mean(phat))^2;
                rsquare_ = 1 - sse / (sse + ssr);
                df=s11.tstat.dfe;
            elseif doODEfit  %do ordinary linear regression
                s = regstats(x_in_log,y_in_log); %ODE regression of ABS, leave out high Atot data
                [p_LR,F_LR] = linhyptest(s.beta, s.covb, 1, [0 1], s.tstat.dfe); %performs an F-test that the slope of linear regression is not sign diff to 1.
                yint=s.beta(1);
                m=s.beta(2);
                rsquare_=s.rsquare;
                p_=p_LR;
                F_=F_LR;
                df=s.tstat.dfe;
            end
        catch err1 %#ok<NASGU>
            %rethrow(err1)
            p_=NaN;F_=NaN;m=NaN;yint=NaN;rsquare_=NaN;sey=NaN;sem=NaN;
        end

        M(n,b)=m;
        Yint(n,b)=yint;
        p_mat(n,b)=p_;
        F_mat(n,b)=F_;
        DF(n,b)=df;
        Rsq_(n,b)=rsquare_;
        StdErr_m(n,b)=sem;
        StdErr_y(n,b)=sey;
            
    end
    ExP=[ExEmPair(1) ExEmPair(2)];
      dataout(2*b-1:2*b,:)=[[1;2] [Noise_F;Noise_F] [Noise_A;Noise_A] [Abs_bias(1);Abs_bias(1)] [Abs_bias(2);Abs_bias(2)] [ExP;ExP] [b;b] M(:,b) StdErr_m(:,b)  ...
          Yint(:,b)   StdErr_y(:,b)  exp(Yint(:,b) ) Rsq_(:,b)  DF(:,b)  p_mat(:,b)];

      %plots for selected wavelength pair
    if DoPlots
        %Figure - Fluorescence vs Atot
        figure(h3)
        subplot(2,4,b),
        plot(At(iiP,j,k),Xt(iiP,j,k),'b.-')
        hold on,plot(At(iiP,j,k),K(iiP,j,k),'ko-','MarkerFaceColor',[0 0 0],'Color',[0 0 0],'LineWidth',1.5)
        hold on,plot(At(iiP,j,k),L(iiP,j,k),'ko-.','Color',[.5 .5 .5],'LineWidth',1.5),hold off
        %xlimits=xlim;ylimits=ylim;
        title(['Case ' num2str(b)]);
        %text(0.05*xlimits(2),0.9*ylimits(2),1,['Case ' num2str(b)]);
        set(gcf,'name',['Dilution series-' TestInfo])
        xlabel('A_T_o_t_a_l'); %legend on the final figure
        if b==max(cases_to_plot); 
            l1=legend('Theory','ACA','CDA'); %legend on the final figure
            set(l1,'position',[0.75 0.35 0.11 0.1])
        end        %Figure - Deviation vs Atot
        if ismember(b,[1 5])
        ylabel('Fluorescence Intensity (R.U.)'); %legend on the final figure
        end
        
        %Figure - Log/Log Fluorescence vs Atot
        figure(h4)
        subplot(2,4,b),
        loglog(At(iiP,j,k),Xt(iiP,j,k),'b.-')
        hold on,loglog(At(iiP,j,k),K(iiP,j,k),'ko-','MarkerFaceColor',[0 0 0],'Color',[0 0 0],'LineWidth',1.5)
        hold on,loglog(At(iiP,j,k),L(iiP,j,k),'ko-.','Color',[.5 .5 .5],'LineWidth',1.5);    hold off
        axis tight
        set(gcf,'name',['Log/Log dilution series -' TestInfo])
        %xlimits=xlim;ylimits=ylim;
        title(['Case ' num2str(b)]);
        %text(0.05*xlimits(2),0.9*ylimits(2),1,['Case ' num2str(b)]);
        xlabel('log(A_T_o_t_a_l)'); %legend on the final figure
        if ismember(b,[1 5])
            ylabel('log(Fluorescence Intensity) R.U.)'); %legend on the final figure
        end
        if b==max(cases_to_plot); 
            l1=legend('Theory','ACA','CDA'); %legend on the final figure
            set(l1,'position',[0.75 0.35 0.11 0.1])
        end

        %Figure - Relative Deviation vs Atot
        figure(h5)
        subplot(2,4,b),
        plot(At(iiP,j,k),(K(iiP,j,k)-Xt(iiP,j,k))./Xt(iiP,j,k),'ko-','MarkerFaceColor',[0 0 0],'Color',[0 0 0],'LineWidth',1.5)
        hold on,plot(At(iiP,j,k),(L(iiP,j,k)-Xt(iiP,j,k))./Xt(iiP,j,k),'ko-.','Color',[.5 .5 .5],'LineWidth',1.5)
        %if ~isempty(yrange); ylim(yrange);end
        xlimits=xlim; %ylimits=ylim;
        hold on,plot(xlimits,[0 0],'b--'),hold off
        if b==1;ylim([-0.05 0.05]),end
        set(gcf,'name',['Relative deviation-' TestInfo])
        title(['Case ' num2str(b)]);
        %text(0.45*xlimits(2),0.9*ylimits(2),1,['Case ' num2str(b)]);
        xlabel('A_T_o_t_a_l'); %legend on the final figure
        if ismember(b,[1 5])
        ylabel('Relative Deviation (R.U.)'); %legend on the final figure
        end
        if b==max(cases_to_plot); 
            l1=legend('ACA','CDA','Theory'); %legend on the final figure
            set(l1,'position',[0.75 0.35 0.11 0.1])
        end
        
        %Figure - sqrt(Deviation) vs Atot
        figure(h6)
        subplot(2,4,b),
        devK=(K(iiP,j,k)-Xt(iiP,j,k))./Xt(iiP,j,k);
        devL=(L(iiP,j,k)-Xt(iiP,j,k))./Xt(iiP,j,k);
        %[Xt(:,j,k) K(:,j,k) L(:,j,k) devK devL];
        plot(At(iiP,j,k),sign(devK).*sqrt(abs(devK)),'ko-','MarkerFaceColor',[0 0 0],'Color',[0 0 0],'LineWidth',1.5)
        hold on,plot(At(iiP,j,k),sign(devL).*sqrt(abs(devL)),'ko-.','Color',[.5 .5 .5],'LineWidth',1.5)
        axis tight
        %if ~isempty(yrange); ylim(yrange);end
        xlimits=xlim;%ylimits=ylim;
        hold on,plot(xlimits,[0 0],'b--');hold off
        if b==1;ylim([-0.02 0.02]),end
        title(['Case ' num2str(b)]);
        %text(0.45*xlimits(2),0.95*ylimits(2),1,['Case ' num2str(b)]);
        set(gcf,'name',['sqrt(rel. deviation)-' TestInfo])
        xlabel('A_T_o_t_a_l'); %legend on the final figure
        if ismember(b,[1 5])
            ylabel('sqrt(Relative Deviation) (R.U.)'); %legend on the final figure
        end
        if b==max(cases_to_plot);
            l1=legend('ACA','CDA','Theory'); %legend on the final figure
            set(l1,'position',[0.75 0.35 0.11 0.1])
        end
        
        %Figure - Absolute Deviation vs Atot
        figure(h7)
        subplot(2,4,b),
        plot(At(iiP,j,k),(K(iiP,j,k)-Xt(iiP,j,k)),'ko-','MarkerFaceColor',[0 0 0],'Color',[0 0 0],'LineWidth',1.5)
        hold on,plot(At(iiP,j,k),(L(iiP,j,k)-Xt(iiP,j,k)),'ko-.','Color',[.5 .5 .5],'LineWidth',1.5)
        %if ~isempty(yrange); ylim(yrange);end
        xlimits=xlim; %ylimits=ylim;
        hold on,plot(xlimits,[0 0],'b--'),hold off
        if b==1;ylim([-0.05 0.05]),end
        set(gcf,'name',['Absolute deviation-' TestInfo])
        title(['Case ' num2str(b)]);
        %text(0.45*xlimits(2),0.9*ylimits(2),1,['Case ' num2str(b)]);
        xlabel('A_T_o_t_a_l'); %legend on the final figure
        if ismember(b,[1 5])
        ylabel('Absolute Deviation (R.U.)'); %legend on the final figure
        end
        if b==max(cases_to_plot); 
            l1=legend('ACA','CDA','Theory'); %legend on the final figure
            set(l1,'position',[0.75 0.35 0.11 0.1])
        end
        
    end
end
figure(h7)

if RunningTheDemo
 disp('-------------------------------------------------');
 disp(' pause ')
end

disp('   ')
disp('In following table, the first row of each case shows ACA-correction');
disp('while the second row of each case shows CDA-correction.');
disp('Note that regression results depend on the number of samples');
disp('in a dilution series and their intensities relative to');
disp('the intensity scale of blanks and noise.');
%disp('    Case    l/l-slope    se     l/l-y_int    se        m         Rsq      df       p(slope=1)');
%disp(dataout(:,8:end));
disp('Case    M       se      B       se      m       Rsq     df p(slope=1)');
fprintf('%d\t\t%2.4f\t%2.4f\t%2.4f\t%2.4f\t%2.3f\t%2.4f\t%d\t%2.3f\n',dataout(:,8:end)');

if RunningTheDemo
 disp('-------------------------------------------------');
 disp(' - end of demo -')
end
%fprintf('%d\t%g\t%g\t%d\t%g\t%d\t%d\t%d\t%2.4f\t%2.4f\t%2.4f\t%2.4f\t%2.3f\t%2.4f\t%d\t%2.3f\n',dataout')

if SaveFigures;
    figprefix=char('F vs Atot','log-log F vs Atot','Relative deviation vs Atot','Sqrt of rel deviation vs Atot','Absolute deviation vs Atot');
    for i=3:7;
        fig=['h' num2str(i)];
        figure(eval(fig));
        FigName=[TestName ' ' deblank(figprefix(i-2,:)) ' ' num2str(i)];
        saveas(gcf,FigName, 'fig');
    end
end

%% Write data to a worksheet
if SaveData;
%     filename='IFE_NoiseTest.xlsx';
%     sheet=[TestName ' ' num2str(ExEmPair)];
%     xlswrite(filename,cellstr(char('Noise_F','Noise_A','A_bias_wave','A_bias'))',sheet,'a1')
%     xlswrite(filename,dataout(1,2:5),sheet,'a2')
%     xlswrite(filename,...
%         cellstr(char('Method','Ex','Em','l/l slope','se slope', 'l/l y-int','se y-int', 'm','rsq_','df','prob slope=1'))',sheet,'a4')
%     xlswrite(filename,cellstr(repmat(char('ACA','CDA'),[max(cases_to_plot),1])),sheet,'a5');
%     xlswrite(filename,dataout(:,6:end),sheet,'b5')
    
    fn='IFE_NoiseTest.dat';
    fileID = fopen(fn,'w');
    if fileID==-1;
        error('IFEtest:fid',['Could not access  ' fn '. It may be a permissions error. Close any files with the same name .'])
    end
    C = cellstr(char('Noise_F','Noise_A','A_bias_wave','A_bias'))';
    fprintf(fileID,'%s\t%s\t%s\t%s\n',C{1,:});
    fprintf(fileID,[repmat('%6.4f\t',[1,3]) '%6.4f\n'],dataout(1,2:5));
    C = cellstr(char('Method','Ex','Em','case','l/l slope','se slope', 'l/l y-int','se y-int', 'm','rsq_','df','prob slope=1'))';
    fprintf(fileID,[repmat('%s\t',[1,11]) '%s\n'],C{1,:});
    C = [cellstr(repmat(char('ACA','CDA'),[max(cases_to_plot),1])) num2cell(dataout(:,6:end))];
    formatSpec = ['%s\t%d\t%d\t%d\t' repmat('%6.4f\t',[1,8])  '\n'];
    [nrows,ncols] = size(C); %#ok<NASGU>
    for row = 1:nrows
        fprintf(fileID,formatSpec,C{row,:});
    end
    fclose(fileID);
    fprintf([fn ' has been written to your current directory...\n'])
end


%% ACA
function [K,IFC]=ABAife(Ex,Em,A,X)
N_samples=size(X,1);
N_em=length(Em);
N_ex=length(Ex);
IFC=NaN*ones([N_samples,N_em,N_ex]);

for i=1:N_samples;
    Atot=zeros(N_em,N_ex);
    Ak=[A(1,:);A(i+1,:)];
    
    Aem=MatchWave(Ak,Em,0);
    Aex=MatchWave(Ak,Ex,0);
    
    for j=1:size(Aem,1)
        for k=1:size(Aex,1)
            Atot(j,k)=Aex(k,2)+Aem(j,2);
        end
    end
    ifc=10.^(1/2*Atot);
    IFC(i,:,:)=ifc;
end
K=IFC.*X;

%% CDA
function [L,H,S] = CDAife(X1,X2,p,e)
% Controlled Dilution (CDA) approach to IFE correction
% REFERENCE: X. Luciani et al. / Chemometrics and Intelligent Laboratory Systems 96 (2009) 227�238
%
% Input Variables:
%   X1 undiluted sample EEM with Ex in the first row and Em in the first column (I3D)
%   X2 diluted EEM (I3Dp) with Ex in the first row and Em in the first column
%   p = dilution factor (p>1)
% Optional input variables
%   e = assumed experimental error, default is e=0.05
%
% Output Variables:
%   L corrected sample EEM, no headers
%   H inner filter matrix
%   S = 1st order approximation of sensitivity
% PROGRAMMER: KR Murphy, UNSW

narginchk(3,4) %check input arguments
if nargin<4;
    e=0.05;
end

%check that X1 and X2 are the same size
if isequal(size(X1),size(X2))
    Xife=X1(2:end,2:end);
    Xd=X2(2:end,2:end);
    L= (((p*Xd).^p)./Xife).^(1/(p-1));     %equ. 21
    H= (Xife./(p*Xd)).^(p/(p-1));          %equ. 22
    S=((p-1-log(p)-log(Xd)+log(Xife)))*e/(p-1)^2;
else
    fprintf('\n Input matrices X1 and X2 are not the same size \n')
    error('Check input files')
end
%% MATCHWAVE
function Y=MatchWave(V1,V2,doplot)
%Y=MatchWave(V1,V2,doplot)
%Automatically match the size of two vectors, interpolating if necessary.
%Vector V1 is resized and interpolated to have the same wavelengths as V2;
%For example, V1 is a correction file (0.5nm intervals) and V2 is an Emission scan in 2 nm intervals.
%Errors are produced if
%(1) there are wavelengths in V2 that are outside the upper or lower limit of V1.
%(2) Be aware of rounding errors. For example, 200.063 is NOT equivalent to 200 nm.
% Errors can often be resolved by restricting the wavelength range of V2
% Copyright K.R. Murphy
% July 2010

if size(V1,2)>2; V1=V1';end  %V1 should have multiple rows
if size(V2,2)>2; V2=V2';end  %V2 should have multiple rows

%Restrict the wavelength range of V1 so it is the same as V2
t=V1(find(V1(:,1)<=V2(1,1),1,'last'):find(V1(:,1)>=V2(end,1),1,'first'),:);
if isempty(t)
    fprintf('\n')
    fprintf(['Error - Check that your VECTOR 1 (e.g. correction file) '...
        'encompasses \n the full range of wavelengths in VECTOR 2 (e.g. emission scan)\n'])
    fprintf('\n Hit any key to continue...')
    pause
    fprintf(['\n VECTOR 1 range is ' num2str(V1(1,1)) ' to ' num2str(V1(end,1)) ' in increments of ' num2str(V1(2,1)-V1(1,1)) '.']),pause
    fprintf(['\n VECTOR 2 range is ' num2str(V2(1,1)) ' to ' num2str(V2(end,1)) ' in increments of ' num2str(V2(2,1)-V2(1,1)) '.']),pause
    fprintf('\n\n This error can usually be resolved by restricting the wavelength range of VECTOR 2,');
    fprintf('\n also, be aware of rounding errors (e.g. 200.063 is NOT equivalent to 200 nm)\n');
    error('Error - abandoning calculations.');
else
    Y=[V2(:,1) interp1(t(:,1),t(:,2),V2(:,1))]; %V1 corresponding with EEM wavelengths
end

if doplot==true;
    figure,
    plot(V1(:,1),V1(:,2)), hold on, plot(Y(:,1),Y(:,2),'ro')
    legend('VECTOR 1', 'VECTOR 2')
end

%% Random number generator
function R=rand_neg1to1(x)
%Matrix of random numbers between -1 to 1
R=2*rand(size(x))-ones(size(x)); 

%% check available toolboxes
function availableFeatures=checktoolboxes(featureStr)
index = cellfun(@(f) license('test',f),featureStr); 
availableFeatures = featureStr(logical(index)); 
if ~isequal(featureStr, availableFeatures)          
 warning('MATLAB:NoLicence','License not found for one or more toolboxes needed to run this program successfully');
 disp('licences needed for')
 disp(featureStr)
 disp('licences found for')
 disp(availableFeatures)
end

