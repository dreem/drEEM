%RestoreDemoInputs
%Restore the original inputs of IFEtest_demo

DemoInputs.Noise_F=3.5000e-04;
DemoInputs.Noise_A=0.0100;
DemoInputs.DilErr=0.0100;
DemoInputs.Abs_bias=[325 -0.0300];
DemoInputs.Abs_bias_type='proportional';
