%% Set up the toolbox
dreeminstall

%% Load data and cut some of the noisy and unreliable wavelengths:
load PortSurveyData_corrected.mat
mydata= assembledataset(XcQS,Ex,Em_in,'QSE','site',sites,...
                'rep',replicates,'longID',filelist_eem,...
                'ID',sampleID,'cruise',cruises,'date',dates,[]);
Xstart=subdataset(mydata,[],mydata.Em>580,mydata.Ex<250);
clearvars -except Xstart

%% Inspect your EEM, locate the scatter
eemreview(Xstart,'samples',10)

%% Part 1: 1st order Raman, 2nd order Rayleigh
eemreview(smootheem(Xstart,[ ],[9 11],[16 14],[ ],[0 0 0 0],[],3382,0))

%% Part 2: 1st order Rayleigh
eemreview(smootheem(Xstart,[7 6],[9 11],[16 14],[ ],[0 0 0 0],[],3382,0))

%% Part 3: 2nd order Raman
eemreview(smootheem(Xstart,[7 6],[9 11],[16 14],[20 4],[0 0 0 0],[],3382,0))

%% Refining the scatter excision
spectralvariance(smootheem(Xstart,[7 6],[9 11],[16 14],[20 4],[0 0 0 0],[],3382,0))
Xtemp=subdataset(smootheem(Xstart,[7 6],[9 11],[16 14],[20 4],[0 0 0 0],[],3382,0),[7:10 86:89],[],[])
spectralvariance(Xtemp)
clearvars Xtemp


Xtemp=subdataset(smootheem(Xstart,[7+5 6+5],[9 11],[16 14],[20 4],[0 0 0 0],[],3382,0),[7:10 86:89],[],[])
spectralvariance(Xtemp)
clearvars Xtemp

Xtemp=subdataset(smootheem(Xstart,[7+5 6+5],[9 11+5],[16 14],[20 4],[0 0 0 0],[],3382,0),[7:10 86:89],[],[])
spectralvariance(Xtemp)
clearvars Xtemp

Xtemp=subdataset(smootheem(Xstart,[7+5 6+5],[9 11+5],[16 14],[20 4+10],[0 0 0 0],[],3382,0),[7:10 86:89],[],[])
spectralvariance(Xtemp)
clearvars Xtemp

%% To interpolate or not to interpolate?
Xtemp=subdataset(smootheem(Xstart,[7+5 6+5],[9 11+5],[16 14],[20 4+10],[1 1 1 0],[],3382,0),[7:10 86:89],[],[])
spectralvariance(Xtemp)
clearvars Xtemp

%% Evaluating our result
ds=subdataset(smootheem(Xstart,[12 11],[9 16],[16 14],[20 14],[0 0 0 0],[],3382,0),[7:10 86:89],[Xstart.Em<300],[])
models=randinitanal(ds,2:5,'starts',2,'constraints','nonnegativity','convgcrit',1e-6);

%% The summary
clearvars
cd '...\\drEEM'
dreeminstall

load PortSurveyData_corrected.mat
mydata= assembledataset(XcQS,Ex,Em_in,'QSE','site',sites,...
                'rep',replicates,'longID',filelist_eem,...
                'ID',sampleID,'cruise',cruises,'date',dates,[]);
Xstart=subdataset(mydata,[],mydata.Em>580,mydata.Ex<250);
clearvars -except Xstart

Xend=smootheem(Xstart,[12 11],[9 16],[16 14],[20 14],[0 0 0 0],[],3382,0);
% Optional
Xend=subdataset(Xend,[7:10 86:89],Xend.Em<300,[]);