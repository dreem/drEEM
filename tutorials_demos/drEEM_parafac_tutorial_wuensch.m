% This is a minimally commented script. refer to the full documentation
% (doc dreem) for more help.

%% Set up the toolbox
cd 'C:\Users\expertuser\somefolder\drEEM\' % Change this!
dreeminstall


%% Load the dataset and visualize the data
load(which('tutorial_dataset2.mat'))
disp(sprintf(DS.description{:}))

eemview(DS,'X',[3 3])
eemreview(DS)
scanview(DS)
spectralvariance(DS)

%% outlier tests

doc outliertest


%model Xin with n = 3:6 components. Apply the nonnegativity constraint.
Test1=outliertest(DS,[],3:6);
outliertest(Test1,3:6)

eemreview(DS,'samples',[20])

DS=zap(DS,20,[],283);

Test2=outliertest(DS,[],3:6);

eemreview(DS,'samples',[5 33])

DS=zap(DS,5,[],267);
DS=zap(DS,33,451,247);
DS=zap(DS,33,327,[]);

Test3=outliertest(DS,[],3:6);

eemreview(DS,'samples',[8])

DS=zap(DS,8,[],315);

Test4=outliertest(DS,[],3:6);

eemreview(DS,'samples',[12 30 40])
DS=zap(DS,12,391,[]);
DS=zap(DS,30,359,[]);
DS=zap(DS,40,327,[]);
DS=zap(DS,40,363,315);

Test5=outliertest(DS,[],3:6);

%% Inspect model parameters
models=randinitanal(DS,2:7,10,'nonnegativity');
%compare spectra for all models
spectralloadings(models,2:7)
loadingsandleverages(models,6)
DS=zap(DS,3,467,[]);
models=randinitanal(DS,2:7,10,'nonnegativity');
loadingsandleverages(models,6)

coreandvar(models)

specsse(models,5)
eemview({models,models},{'X','Model5','error_residuals'},[4 3],[],[],{'data','model','error'},[1 1 0.3],[],'colorbar')
%view score:score plots for the 6-component model
compcorrplot(models,5)

DSn=normeem(DS);
modelsn=randinitanal(DSn,4:6,10,'nonnegativity');
compcorrplot(modelsn,5)

%% Validate the model
LSmodel=randinitanal(DS,5,100,'nonnegativity');
DSsplit=splitds(DS,[],6,'random',{[1 2],[3 4],[5 6]})
splitmodels=splitanalysis(DSsplit,5,50,'nonnegativity'); 
val_results=splitvalidation(splitmodels,5,[],[],LSmodel);
close all

%% Export the model

[FMax,B,C] = modelout(val_results,5,'pure_5_validated.xlsx');

openfluor(val_results,5,'pure_5_validated_of.txt')

openfluormatches("C:\Users\wuensch\Downloads\OpenFluorSearch__pure_5_validated_of_20200708.csv")