%% Set up the toolbox
dreeminstall
unzip('dreem_demofiles_2.zip',fileparts(which('dreem_demofiles_2.zip')))

datalocation = [fileparts(which('dreem_demofiles_2.zip')),filesep,'demofiles_AL_HYI'];
savelocation = 'C:\Work\folder1\imported\Xin.mat'; % Where should the dataset be saved to?
flusize = 'A2..DR126'; % EEMs limits, open in Excel and note where the EEM begins and ends.
abssize = 'A4..J124';   % Absorbance limits, open in Excel and locate the absorbance column.

%% Read in files
cd(datalocation);
[X,Emmat,Exmat,fl,outdata]=readineems(3,'*Waterfall Plot Sample.dat',flusize,[0 1],0,0);
DS = assembledataset(X,Exmat(1,:),Emmat(:,1),'RU','filelist',fl,[]);
clearvars -except DS datalocation savelocation flusize abssize

[X_b,Emmat_b,Exmat_b,fl_b,outdata_b]=readineems(3,'*Waterfall Plot Blank.dat',flusize,[0 1],0,0);
DSb = assembledataset(X_b,Exmat_b(1,:),Emmat_b(:,1),'RU','filelist',fl_b,[]);
clearvars -except DS DSb datalocation savelocation flusize abssize

[S_abs,W_abs,wave_abs,filelist_abs]=readinscans('Abs','dat_1_10',abssize,0,0,'* - Abs Spectra Graphs');

A=[wave_abs;S_abs];          %add wavelengths to Absorbance scans
Abs.A=A;                     %wavelengths are in the first row. They will be removed later.
Abs.filelist=cellstr(filelist_abs);
Abs.wave=wave_abs;
clearvars -except DS DSb Abs datalocation savelocation flusize abssize


%% Check file alignment
try
    disp([cellstr(num2str((1:DS.nSample)')) DS.filelist DSb.filelist])
catch
    error('Missmatch: DS.nSample, DS.filelist, DSb.filelist')
end
try
    disp([DS.filelist DSb.filelist Abs.filelist])
catch
    error('Missmatch: DS.filelist, DSb.filelist, Abs.filelist')
end
try
    disp([cellstr(num2str((1:DS.nSample)')) DS.filelist DSb.filelist Abs.filelist])
catch
    error('Missmatch: DS.nSample, DS.filelist, DSb.filelist, Abs.filelist')
end
warning('Check that the samples are aligned, press enter when ready')
pause;

%% Resize EEMs depending on absorbance data
disp('Abs wavelengths: min and max')
AbsRange=[min(Abs.wave) max(Abs.wave)];
disp(AbsRange)
disp('EEM wavelengths: min and max')
EEMRange=[min([DS.Em' DS.Ex']) max([DS.Em' DS.Ex'])];
disp(EEMRange)

Xin=subdataset(DS,[],...
    DS.Em<AbsRange(1) | DS.Em>AbsRange(2),...
    DS.Ex<AbsRange(1) | DS.Ex>AbsRange(2));
B=subdataset(DSb,[],...
    DS.Em<AbsRange(1) | DS.Em>AbsRange(2),...
    DS.Ex<AbsRange(1) | DS.Ex>AbsRange(2));

%% Correct EEMs
RamanWavel=351;
Sr=squeeze(B.X(:,:,DSb.Ex==351));
W=[B.Em';Sr];              %2D Matrix of matched (350 nm) Raman scans
B.W=W;


[XcRU Arp IFCmat BcRU XcQS QS_RU]=fdomcorrect(Xin.X,Xin.Ex,Xin.Em,...
    [Xin.Em ones(size(Xin.Em))],[Xin.Ex ones(size(Xin.Ex))],...
    B.W,[351 381 426],Abs.A,B.X,B.W,[],[]);



%% Consolidate the data 
Xin.X=XcRU;             	%X is in Raman Units
Xin.RamanArea=Arp;          %Raman Peak Area
Xin.IFE=IFCmat;             %IFE correction factors
Xin.Abs_wave=Abs.A(1,:);    %Abs wavelengths
Xin.Abs=Abs.A(2:end,:);     %Abs data
Xin.Ram_wave=B.Em';      %Water Raman wavelengths
Xin.Emcor='internally corrected by AquaLog'; %corrected by AquaLog not drEEM
Xin.Excor='internally corrected by AquaLog'; %corrected by AquaLog not drEEM
Xin.RamOpt=[350 381 426];   %Raman integration range on Em spectrum
Xin.RamSource='Water Raman scans extracted from blanks';

clearvars -except Xin


checkdataset(Xin)

%% Save the data

% save(savelocation,'Xin')