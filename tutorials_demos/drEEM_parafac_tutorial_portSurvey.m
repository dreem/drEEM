% This is a minimally commented script. refer to the full documentation
% (doc dreem) for more help.

%% Set up the toolbox
cd 'C:\Users\expertuser\somefolder\drEEM\' % Change this!
dreeminstall


%% Assemble dataset, smooth EEM scatter, and remove outliers
load(which('PortSurveyData_corrected.mat'))

mydata= assembledataset(XcQS,Ex,Em_in,'QSE',...
		'site',sites,...
		'rep',replicates,...
		'longID',filelist_eem,...
		'ID',sampleID,...
		'cruise',cruises,...
		'date',dates,[]);

    
disp(mydata)
classinfo(mydata)
eemview(mydata,'X',[3 3],[],[],'site')

%%
SubData=subdataset(mydata,[],mydata.Em>600,mydata.Ex<250);

Xs = smootheem(SubData,[12 11],[9 16],[16 14],[20 14],[0 0 0 0],[],3382,'pause');
Xs = smootheem(SubData,[12 11],[9 16],[16 14],[20 14],[0 0 0 0],[],3382,0);

eemview(Xs,'X',[3 3],[],[],'site')
Xin=subdataset(Xs,{'site','','0A'},[],[]);

eemview(Xin,'X',[1 3],176,[],[],[],[],[],20)
Xin=zap(Xin,176,[],[345 350]);

eemview(Xin,'X',[1 3],176,[],[],[],[],[],20)

%%
Test1=outliertest(Xin,[2,2],3:7);
Test1u=randinitanal(Xin,3:7,'constraints','unconstrained','convgcrit',1e-4,'starts',2);
outliertest(Test1u,[2,2],3:7)

compcorrplot(Test1,4)
compcorrplot(Test1,5)
compcorrplot(Test1,6)

compcorrplot(Test1,6,[],'site')

Xpre=normeem(Xin);

Test1p=outliertest(Xpre,[2,2],4:7);


compcorrplot(Test1p,6)

outliertest(Test1p,[2,2],4:7);

out=ismember(Xin.i,[7 8 9 10 11 86 87 88 89 215 224]);
Xin=subdataset(Xin,out,[],[]);

Xpre=normeem(Xin);
Test2=outliertest(Xpre,[2,2],5:7);


eemview({Test2, Test2},{'X','Model6','error_residuals'},[1 3],49)

compare2models(Test2,5,6,0.05)

spectralloadings(Test2,5:7)

specsse(Test2,5:7)

%%
[LSmodel6,convg6,DSit6]=randinitanal(Xpre,5:7,10,'nonnegativity');
[LSmodel6,convg6,DSit6]=randinitanal(Xpre,5:7,10,'nonnegativity',1e-8);


spectralloadings(LSmodel,6)
comparespectra(LSmodel,6)
fingerprint(LSmodel,6)

loadingsandleverages(LSmodel,6)
eemview({LSmodel, LSmodel},{'X','Model6','error_residuals'},[2 3])

%%
doc splitds
S1=splitds(Xpre,[],4,'alternating',{[1 2],[3 4],[1 3],[2 4],[1 4],[2 3]});

A1=splitanalysis(S1,5:6,10,'nonnegativity');
% Or (since v0.6.0)
A1=splitanalysis(S1,5:6,'starts',10,'constraints','nonnegativity');
