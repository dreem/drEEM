function [cdom,fdom] = spectralvariance(data)
%
% <strong>Syntax</strong>
%   [cdom,fdom] = <strong>spectralvariance</strong>(data)
%
% <a href="matlab: doc spectralvariance">help for spectralvariance</a> <- click on the link



% Plot the spectral variability of CDOM and FDOM to explore raw data
% The function scales all data to unit variance in the sample mode to give
% each sample equal weighting.
%
% [cdom,fdom] = spectralvariance(data)
%
%INPUT VARIABLES: 
% data:        One data structures to be plotted.
%
%EXAMPLES
% 1.   [cdom,fdom] = spectralvariance(data)
%
% Notice:
% This mfile is part of the drEEM toolbox. Please cite the toolbox
% as follows:
%
% Murphy K.R., Stedmon C.A., Graeber D. and R. Bro, Fluorescence
%     spectroscopy and multi-way techniques. PARAFAC, Anal. Methods, 2013, 
%     DOI:10.1039/c3ay41160e. 
%
% spectralvariance: Copyright (C) 2019 Urban J. Wuensch
% Chalmers University of Technology
% Sven Hultins Gata 6
% 41296 Gothenburg
% Sweden
% $ Version 0.1.0 $ March 2019 $ First Release

%%


if isfield(data,'X')&isfield(data,'Abs')
    pltcase=1;
elseif isfield(data,'X')&~isfield(data,'Abs')
    pltcase=2;
elseif isfield(data,'Abs')&~isfield(data,'X')
    pltcase=3;
else
    pltcase=10;
end

if pltcase==10
   error('Refer to function help. Could not find necessary fields in the dataset.')
end



if pltcase==1|pltcase==2
    [X,~,fscales]=nprocess(data.X,[0 0 0],[1 0 0]);
    fdom=squeeze(nanstd(X));
end
if pltcase==1|pltcase==3
    [Y,~,ascales]=nprocess(data.Abs,[0 0],[1 0]);
    cdom=nanstd(Y);
end
fscales{1}(isinf(fscales{1}))=nan;
if any(fscales{1}>5*median(fscales{1}))
    warning('Some samples have very little signal (e.g. blanks) and thus likely negatively impact the results')
    samples=fscales{1}>20*median(fscales{1});
    disp(['Consider removing the sample(s):  ',num2str(find(fscales{1}'>5*median(fscales{1}')))])
    disp('If no blanks (or similar) samples are present, the issue may be samples with high fluorescence values instead.')
end

%%
hf=dreemfig;
set(hf,'unit','normalized');
pos=get(hf,'Position');
set(hf,'Position',[pos(1:2) 0.5 0.3]);
movegui('center');
ax1=subplot(4,5,[1 2 6 7 11 12]);
if pltcase==1|pltcase==3
    h1=plot(data.Abs_wave,Y./sum(Y,2),'Color',[0.5 0.5 0.5],'LineWidth',0.2);
    hold on
    h2=plot(data.Abs_wave,nanstd(Y)./sum(nanstd(Y)),'Color','k','LineWidth',2);
    legend([h1(1), h2],{'All spectra','Std. dev.'})
end
axis tight
xlabel('Absorbance wavelength (nm)')
ylabel('Std. dev. absorbance')
title('CDOM absorbance')


ax2=subplot(4,5,[4 5 9 10 14 15]);
if pltcase==1|pltcase==2
    pltdata=squeeze(nanstd(X));
    pltdata(pltdata==0)=nan;
    contourf(data.Ex,data.Em,pltdata,25)
end
c=colorbar;
ylabel(c,'Std. dev. fluorescence')
title('FDOM fluorescence')
set(gca,'units','pixel')
set(gca,'YTickLabel','','XTickLabel','')
refylim=get(ax2,'YLim');
refxlim=get(ax2,'XLim');

% [left bottom width height]

ax3=subplot(4,5,[3 8 13]);
if pltcase==1|pltcase==2
    pltvec=squeeze(nansum(X,3))./(mean(max(squeeze(nansum(X,3))))*2);
    pltvec(pltvec==0)=nan;
    
    plot(pltvec,data.Em,'Color',[.5 .5 .5],'LineWidth',0.2)
    hold on
    plot(nansum(squeeze(nanstd(X)),2)./max(nansum(squeeze(nanstd(X)),2)),data.Em,'Color','k','LineWidth',2)
end
set(gca,'XDir','reverse')
set(gca,'units','pixel')
refpos=get(ax2,'pos');
pos1=get(ax3,'pos');
ylim(refylim)
set(ax3,'XTickLabel','')

offs=refpos(1)-(pos1(1)+pos1(3));
set(ax3,'pos',[pos1(1)+offs refpos(2) pos1(3) refpos(4)])
ylabel('Emission (nm)')

ax4=subplot(4,5,[19 20]);
if pltcase==1|pltcase==2
    pltvec=squeeze(nansum(X,2))./(mean(max(squeeze(nansum(X,2))))*2);
    pltvec(pltvec==0)=nan;
    
    plot(data.Ex,pltvec,'Color',[.5 .5 .5],'LineWidth',0.2)
    hold on
    plot(data.Ex,nansum(squeeze(nanstd(X)))./max(nansum(squeeze(nanstd(X)))),'Color','k','LineWidth',2)
end
set(gca,'units','pixel')
xlim(refxlim)
set(ax4,'YTickLabel','')
refpos=get(ax2,'pos');
pos2=get(ax4,'pos');
offs=refpos(2)-(pos2(2)+pos2(4));
set(ax4,'pos',[refpos(1) pos2(2)+offs refpos(3) pos2(4)])
xlabel('Excitation (nm)')

pos2=get(ax4,'pos');
height2=pos2(4);
pos1=get(ax3,'pos');
height1=pos1(3);
diff=height1-height2;
set(ax3,'pos',[pos1(1)+diff pos1(2) pos1(3)-diff pos1(4)])

set(ax1,'units','normalized')
set(ax2,'units','normalized')
set(ax3,'units','normalized')
set(ax4,'units','normalized')

dreemfig(hf);
end
