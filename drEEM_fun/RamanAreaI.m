function [Y,BaseArea]=RamanAreaI(M,EmMin,EmMax)
%
% <strong>Syntax</strong>
%   [Y,BaseArea]=<strong>RamanAreaI</strong>(M,EmMin,EmMax)
%
% <a href="matlab: doc RamanAreaI">help for RamanAreaI</a> <- click on the link


% [Y,EmMin,EmMax]=RamanAreaI(M,EmMin,EmMax)
% Find the area under the curves in M between wavelengths EmMin and EmMax, 
% data are interpolated to 0.5 nm intervals

%interpolate to 0.5 nm intervals
waveint=0.5; %nm
waves=(EmMin:waveint:EmMax)';
Mpt5 = FastLinearInterp(M(1,:)', M(2:end,:)', waves)'; %faster linear interp
%figure, plot(Mpt5)
%Mpt5 = interp1(M(1,:)', M(2:end,:)', waves,'spline')'; %built in alternative

%integrate
RAsum=trapz(waves,Mpt5,2);%;
BaseArea=(EmMax-EmMin)*(Mpt5(:,1)+0.5*(Mpt5(:,end)-Mpt5(:,1)));

%RAsum=trapz(waves,Mpt5,2)*waveint;%;
%BaseArea=(EmMax-EmMin)*(Mpt5(:,1)+0.5*(Mpt5(:,end)-Mpt5(:,1)))*waveint;
% disp([RAsum BaseArea BaseArea./RAsum]);
Y = RAsum;% - BaseArea;
end

function Yi = FastLinearInterp(X, Y, Xi)
%by Jan Simon
% X and Xi are column vectros, Y a matrix with data along the columns
[dummy, Bin] = histc(Xi, X);  %#ok<HISTC,ASGLU>
H            = diff(X);       % Original step size
% Extra treatment if last element is on the boundary:
sizeY = size(Y);
if Bin(length(Bin)) >= sizeY(1)
    Bin(length(Bin)) = sizeY(1) - 1;
end
Xj = Bin + (Xi - X(Bin)) ./ H(Bin);
% Yi = ScaleTime(Y, Xj);  % FASTER MEX CALL HERE
% return;
% Interpolation parameters:
Sj = Xj - floor(Xj);
Xj = floor(Xj);
% Shift frames on boundary:
edge     = (Xj == sizeY(1));
Xj(edge) = Xj(edge) - 1;
Sj(edge) = 1;           % Was: Sj(d) + 1;
% Now interpolate:
if sizeY(2) > 1
    Sj = Sj(:, ones(1, sizeY(2)));  % Expand Sj
end
Yi = Y(Xj, :) .* (1 - Sj) + Y(Xj + 1, :) .* Sj;
end