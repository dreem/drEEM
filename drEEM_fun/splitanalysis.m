function dataout=splitanalysis(data,f,varargin)
%
% <strong>Syntax</strong>
%   [dataout] = <strong>splitanalysis</strong>(data,f,Name,Value)
%   [dataout] = <strong>splitanalysis</strong>(data,f,it,constraints,convgcrit)
%
% <a href="matlab: doc splitanalysis">help for splitanalysis</a> <- click on the link

% Generate PARAFAC models nested in dataset splits.
% The function automatically detects whether parallelization can ocur to
% speed up the fitting process by utilizing all physical CPU cores.
%
%
% Notice:
% This mfile is part of the drEEM toolbox. Please cite the toolbox
% as follows:
%
% Murphy K.R., Stedmon C.A., Graeber D. and R. Bro, Fluorescence
%     spectroscopy and multi-way techniques. PARAFAC, Anal. Methods,
%     5, 6557-6566, 2013. DOI:10.1039/c3ay41160e.
%
% splitanalysis: Copyright (c) 2019 Urban W�nsch
% Water Environment Technology
% Chalmers University of Technology
% Department of Civil and Environmental Engineering
% Water Environment Technology
% Sven Hultins gata 6
% 412 96 Gothenburg
% wuensch@chalmers.se
%
% $ Version 0.1.0 $ August 2019 $ First Release
%% Input argument handling
if nargin==0
    help splitanalysis
    return
end

% Decide whether or not to use input parser
parseargs={'constraints','starts','convgcrit','maxit','tbox','init','mode'};
args=varargin;
for n=1:numel(args)
    if isnumeric(args{n})
        args{n}=num2str(args{n});
    end
end
if numel(varargin)>0
    if any(contains(cellstr(string(args)),parseargs)) % Name,Value-scheme is used
        parser=true;
    else
        parser=false;
    end
else
    parser=true;
end

if parser % This is true if pair option -> value is supplied
    try
        params = inputParser;
        params.addParameter('constraints', 'nonnegativity', @checkconstraints);
        params.addParameter('starts', 40, @isnumeric);
        params.addParameter('convgcrit', 1e-6, @isnumeric);
        params.addParameter('maxit', 2500, @isnumeric);
        params.addParameter('tbox', 'nway', @checktoolbox);
        params.addParameter('init', 'random', @checkinit);
        params.addParameter('mode', 'default', @ischar);
        params.parse(varargin{:});
    catch ME
        help splitanalysis
        error('Function input not understood. Please see text above for help on input specification.')
    end
    constraints = params.Results.constraints;
    convgcrit  = params.Results.convgcrit;
    maxit  = params.Results.maxit;
    tbox = params.Results.tbox;
    starts = params.Results.starts;
    initstyle = params.Results.init;
    fmode = params.Results.mode;
else % otherwise, old-style inputs are supplied
    % Try to parse these inputs manually
    if numel(varargin)>=1
        starts = varargin{1};
        if isempty(starts);starts=40;end
    else
        starts=40;
    end
    if numel(varargin)>=2
        constraints = varargin{2};
        if isempty(constraints);constraints='nonnegativity';end
    else
        constraints = 'nonnegativity';
    end
    if numel(varargin)>=3
        convgcrit = varargin{3};
        if isempty(convgcrit);convgcrit=1e-6;end
    else
        convgcrit = 1e-6;
    end
    maxit  = 2500;
    tbox = 'nway';
    initstyle = 'random';
    fmode = 'default';
end
% Check that the inputs (if supplied) were correct
if ~isnumeric(starts)
    error('Input to ''it'' must be numeric')
end
if ~isnumeric(convgcrit)||convgcrit>1e-2||convgcrit<1e-15
    error('Input to ''constraints'' is either not numeric, too small, or too big.')
end
if ~any([strcmp(constraints,'nonnegativity') strcmp(constraints,'unconstrained') strcmp(constraints,'unimodnonneg')])
    error('Input to ''constraints'' must be ''unconstrained'', ''nonnegativity'', or ''unimodnonneg''')
end



%% Setup
funmode=parallelcomp;
[oldp,newp]=tboxinit(tbox);
opt = setoptions(tbox,constraints,convgcrit,maxit,initstyle);

fac=f;
nsplit=numel(data.Split);
nfac=numel(fac);
numstarts=nsplit*starts*numel(fac);
facCalls=reshape(repmat(fac,starts*nsplit,1),numstarts,1);
splitsource=reshape(repmat((1:nsplit),starts,nfac),numstarts,1);


%% Welcome messages
disp('  ')
disp('-----')
disp(' splitanalysis.m - Generate PARAFAC models of split datasets')
disp('-----')
switch tbox
    case 'pls'
        disp(['PARAFAC engine:                  ','PLS_toolbox'])
    case 'nway'
        disp(['PARAFAC engine:                  ','N-way toolbox'])
end
switch funmode
    case 'sequential'
        disp(['Parallelization:                 ','no'])
    case 'parallel'
        disp(['Parallelization:                 ','yes'])
end
disp(['Models with components:          [',num2str(f),']'])
disp(['Number of random starts:         ',num2str(starts)])
disp(['Convergence criterion:           ',num2str(convgcrit)])
disp(['Maximum number of iterations:    ',num2str(maxit)])
disp(['Constraint:                      ',constraints])
disp(['% missing numbers:               ',num2str(round( (sum(isnan(data.X(:)))./numel(data.X))*100 ,3))])
disp('-----')
disp('  ')
%% Submit the models
ttot=tic;
switch funmode
    case 'sequential'
%         modout(1:nItTot) = struct('loads',{},'detail',{}); % Preallocate output
        h=waitbar(0,'PARAFAC models are being calculated');
        for i=1:numstarts
            modout(i)=dreemparafac(data.Split(splitsource(i)).X,facCalls(i),opt,tbox); %#ok<AGROW>
            try
            h=waitbar(i/numstarts,h,['PARAFAC models are being calculated [',num2str(i),'/',num2str(numstarts),']']);
            catch
                disp('Waitbar was closed, but function will keep running.')
            end
        end
        close(h)
        Model={modout.loads}';
        Err=arrayfun(@(x) x.detail.ssq.residual,modout,'UniformOutput',false)';
        Iter=arrayfun(@(x) x.detail.critfinal(3),modout,'UniformOutput',false)';

    case 'parallel'
        modout(1:numstarts) = parallel.FevalFuture; % Preallocate output
        for i=1:numstarts
            modout(i)=parfeval(@dreemparafac,1,data.Split(splitsource(i)).X,facCalls(i),opt,tbox);
        end
        [Model,Iter,Err,ttc] = trackprogress(modout,numstarts,tbox,splitsource,facCalls);
end

restoreoldpath(oldp,newp); % In case matlabpath was changed, restore it.
%% Retreive results
dataout=data;
for ii=f
    mname=['Model' int2str(ii)];
    Ffield=mname;
    Ifield=[mname,'_it'];
    Efield=[mname,'_err'];
    Ifieldi=[mname,'_ByRuns_it'];
    Efieldi=[mname,'_ByRuns_err'];
    for jj=1:nsplit
        tidx=facCalls==ii&splitsource==jj;   % Index of all f's in each split
        if ~any(isnan(cell2mat(Err(tidx))))
            [~,midx]=min(cell2mat(Err(tidx)));   % Lowest SSE solution
            miniIdx=find(tidx);                  % find indices of tidx
            miniIdx=miniIdx(midx);               % locate Best solution in all targets.

            dataout.Split(jj).(Ffield)=Model{miniIdx};
            dataout.Split(jj).(Ifield)=Iter{miniIdx};
            dataout.Split(jj).(Efield)=Err{miniIdx};
            dataout.Split(jj).(Ifieldi)=[Iter{tidx}];
            dataout.Split(jj).(Efieldi)=[Err{tidx}];
        else
            dataout.Split(jj).(Ffield)={rand(dataout.Split(jj).nSample,ii) rand(dataout.Split(jj).nEm,ii) rand(dataout.Split(jj).nEx,ii)};
            dataout.Split(jj).(Ifield)=nan;
            dataout.Split(jj).(Efield)=nan;
            dataout.Split(jj).(Ifieldi)=nan;
            dataout.Split(jj).(Efieldi)=nan;
        end
    end
end

dataout.Split_AnalRuns=starts;
dataout.Split_PARAFAC_Initialise=initstyle;
dataout.Split_PARAFAC_constraints=constraints;
dataout.Split_PARAFAC_convgcrit=convgcrit;


%% Finish up
switch funmode
    case 'parallel'
    ttot=toc(ttot);
    ttc=sum(seconds(ttc));
    disp(' ')
    disp('Finished.')
    disp(['Time elaped: ',num2str(round(ttot./60,2)),'min. Parallel computing speedup: x',num2str(round(ttc/ttot,2))])
    disp(' ');
 case 'sequential'
    ttot=toc(ttot);
    disp(' ')
    disp('Finished.')
    disp(['Time elaped: ',num2str(round(ttot./60,2)),'min.'])
end


end

%% Internal functions used above (shared with randinitanal)
%%
function checkconstraints(finput)
if ~contains(finput,{'unconstrained', 'nonnegativity', 'unimodnonneg'})
    error('Input to ''constraints'' must be ''unconstrained'', ''nonnegativity'', or ''unimodnonneg''')
end
end

%%
function checkinit(finput)
if ~contains(finput,{'svd', 'random','multiplesmall'})
    error('Input to ''init'' must be ''svd'' or ''random''.')
end
end

%%
function funmode=parallelcomp
test=ver;
funmode='sequential';
if any(contains({test.Name},'Parallel'))
    funmode='parallel';
    try
        initppool
    catch
        funmode='sequential';
    end
end
end

%%
function checktoolbox(finput)
if ~contains(finput,{'nway', 'pls'})
    error('Input to ''tbox'' must be ''nway'', or ''pls''. Other software products are currently not supported.')
end
end

%%
function initppool
warning off
try
    poolsize=feature('NumCores');
    p = gcp('nocreate'); % If no pool, do not create new one.
    if isempty(p)
        parpool('local',poolsize);
    elseif p.NumWorkers~=poolsize
        disp('Found existing parpool with wrong number of workers.')
        disp(['Will now create pool with ',num2str(poolsize),' Workers.'])
        delete(p);
        parpool('local',poolsize);
    else
        disp(['Existing parallel pool of ',num2str(p.NumWorkers),' worker found and used...'])
    end
catch ME
    rethrow(ME)
end
warning on
end

%%
function [oldp,newp]=tboxinit(tbox)
warning off
oldp = path;
mlpath = path;mlpath=textscan(mlpath,'%s','delimiter',pathsep);mlpath=mlpath{:};
nway=contains(mlpath,'drEEM');
plsp=contains(mlpath,'PLS');

switch tbox
    case 'nway'
        if find(nway,1,'first') > find(plsp,1,'first')
            rmpath(mlpath{nway|plsp});
            addpath(mlpath{plsp});
            addpath(mlpath{nway});
        end
    case 'pls'
        if find(nway,1,'first') < find(plsp,1,'first')
            rmpath(mlpath{nway|plsp});
            addpath(mlpath{nway});
            addpath(mlpath{plsp});
        end
        clearvars pls
        try
            pls test
            disp('Testing PLS_toolbox'),evridebug
        catch
            error('PLS_toolbox either not installed or not functional.')
        end
    otherwise
        error('That''s a toolbox I don''t know about.')
end
newp = path;
warning on
end

%%
function opt = setoptions(tbox,constraints,convgcrit,maxIt,initstyle)
if contains(tbox,'pls')
    opt=parafac('options');
    opt.plots='none';
    opt.waitbar='off';
    if contains(constraints,'nonnegativity')
        % Set default: All dims nonnegative
        cdim=[1:3];
        % Check for custom input (deletes default)
        if ~isempty(erase(constraints,'nonnegativity'))
            t=(erase(constraints,'nonnegativity'));
            cdim=[];
            for n=1:numel(t)
                cdim=[cdim str2double(t(n))];
            end
            if numel(cdim)>3||any(cdim>3)
                error('numeric input to nonnegativity not understood')
            end
        end
        for i=cdim;opt.constraints{i}.type='nonnegativity';end
    elseif contains(constraints,'unimodality')
        for i=[1 3];opt.constraints{i}.type='nonnegativity';end
        opt.constraints{2}.type='unimodnonneg';
    end
    if contains(initstyle,'svd')
        opt.init=2;
    elseif contains(initstyle,'random')
        opt.init=3;
    end
    opt.stopcriteria.iterations=maxIt;
    opt.stopcriteria.relativechange=convgcrit;
elseif contains(tbox,'nway')
    if contains(constraints,'nonnegativity')
        % Set default: All dims nonnegative
        cdim=[1:3];
        % Check for custom input (deletes default)
        if ~isempty(erase(constraints,'nonnegativity'))
            t=(erase(constraints,'nonnegativity'));
            cdim=[];
            for n=1:numel(t)
                cdim=[cdim str2double(t(n))];
            end
            if numel(cdim)>3||any(cdim>3)
                error('numeric input to nonnegativity not understood')
            end
        end
        for i=cdim;opt.constraints{i}.type=2;end
        for i=setdiff(1:3,cdim);opt.constraints{i}.type=0;end
        
    elseif contains(constraints,'unimodnonneg')
        for i=[1 3];opt.constraints{i}.type=2;end
        opt.constraints{2}.type=3;
    elseif contains(constraints,'unconstrained')
        for i=1:3;opt.constraints{i}.type=0;end
    end
    if contains(initstyle,'svd')
        opt.init=1;
    elseif contains(initstyle,'random')
        opt.init=2;
    elseif contains(initstyle,'multiplesmall')
        opt.init=10;
    end
    opt.stopcriteria.iterations=maxIt;
    opt.stopcriteria.relativechange=convgcrit;
end

end

%%
function [out] = dreemparafac(tensor,f,opt,tbox)
% Start the PARAFAC model. PLS_toolbox reads a pref-file, which is tricky if
% that's done in parallel. This here is to catch errors related to that.
switch tbox
    case 'pls'
        tries=0;
        while tries<100
            try
                disp(datetime)
                out=parafac(tensor,f,opt);
                tries=102;
            catch ME
                tries = tries+1;
                pause(rand(1)/16);
                if tries>100
                    rethrow(ME)
                end
            end
        end
    case 'nway'
        [Factors,it,err,~] = nwayparafac(tensor,f,...
            [opt.stopcriteria.relativechange opt.init 0 0 50 opt.stopcriteria.iterations],...
            [opt.constraints{1}.type opt.constraints{2}.type opt.constraints{3}.type]);
        
        out.loads=Factors;
        out.detail.critfinal(3)=it;
        out.detail.ssq.residual=err;
        clearvars Factors it err
        
end
end

%% 
function [hittype,printinfo] = scandiary(diary,tbox)            %hittype
switch tbox
    case 'nway'
        targets={'PRELIMINARY',2,[];...                                 %1
            'Sum-of-Squares   Iterations  Explained',2,1:5;...          %2
            'The algorithm converged',0,[];...                          %3
            'Warning: Matrix is close to singular ',0,[];...            %4
            'The misfit is approaching the machine uncertainty',0,[];...%5
            'WARNING, SOME FACTORS ARE HIGHLY CORRELATED.',0,[]};       %6
    case 'pls'
        targets={'Fitting new PARAFAC ...',2,[];...                     %1
            'plsupdate',0,[];};                                         %2
end

    
hittype=0;
printinfo='';
for n=1:size(targets,1)
    if ~strcmp(targets{n},'plsupdate')
        res=strfind(diary,targets{n});
        res=find(not(cellfun('isempty',res)));
        if ~isempty(res)
            try
                printinfo=diary{res+targets{n,2}};
                hittype=n;
            catch
                printinfo='';
                hittype=0;
            end
            return
        end
    elseif strcmp(targets{n},'plsupdate')
        idx=not(cellfun('isempty',cellfun(@str2num,diary,'UniformOutput',false)));
        if any(idx)
            printinfo=diary{idx};
            hittype=2;
        end
    end
end
end

%%
function [diariesOld] = printupdates(diaries,diariesOld,modelID,tbox)
% Function for reading futures Diary of parfeval futures
if numel(diariesOld)~=numel(diaries)
    sizeOld=numel(diariesOld); 
    sizeNew=numel(diaries); 
    dispIdx=(sizeOld+1):(sizeNew-1);
    [hittype,printinfo] = scandiary(diaries(dispIdx),tbox);
    switch hittype
        case 0 % Nothing to disply
        case 1 % New model
            disp(['mod-id ',sprintf('%03d',modelID),':  Model initialized. ',printinfo])
        case 2 % Display prelim. fit numbers
            switch tbox
                case 'nway'
                    fitinfo=str2num(printinfo);
                    if ~isempty(fitinfo)
                        disp(['mod-id: ',     sprintf('%03d',modelID),...
                            ':    it. ',    sprintf('%-*s',4,num2str(fitinfo(2))),...
                            '     err.: ', sprintf('%.3f',(round(fitinfo(1),3))),...
                            '     %exp.: ',  sprintf('%.2f',(round(fitinfo(3),2)))]);
                    end
                case 'pls'
                    fitinfo=str2num(printinfo);
                    if ~isempty(fitinfo)
                        disp(['mod-id: ',sprintf('%03d',modelID),...
                            ':    it.: ',sprintf('%-*s',4,num2str(fitinfo(1))),...
                            '     err.: ',sprintf('%.3f',(round(fitinfo(4),3))),...
                            '     chgfit.: ',sprintf('%.2u',fitinfo(3))]);
                    end
                    
            end
        case 4 % warning issued
        msgbox({'Warning during PARAFAC fitting: ',...
            'Warning: Matrix is close to singular or badly scaled.',...
            'Results may be inaccurate',' ',...
            'Acknowledge this message by clicking ''ok''';},...
            'WARNING','warn','replace');
        case 5 % warning issued
        msgbox({'Warning during PARAFAC fitting: ',...
            'The misfit is approaching the machine uncertainty.',...
            'If pure synthetic data is used this is OK. Otherwise',...
            'multiply the EEMs by a large number',' ',...
            'Acknowledge this message by clicking ''ok''';},...
            'WARNING: Machine Precision','warn','replace');
        case 6 % warning issued
        msgbox({'Warning during PARAFAC fitting: ',...
            'SOME FACTORS ARE HIGHLY CORRELATED.',...
            'Consider modeling fewer components.',...
            'Acknowledge this message by clicking ''ok''';},...
            'WARNING: Correlated factors','warn','replace');
    end
    diariesOld=diaries;
end
end

%%
function restoreoldpath(oldp,newp)
    if ~isequal(oldp,newp)
        path(oldp)
    end
end

function [vout] = rcvec(v,rc)
% Make row or column vector
% v: vector
% rc: either 'row' ([1:5])or 'column' ([1:5]')
sz=size(v);
if ~any(sz==1)
    error('Input is not a vector')
end

switch rc
    case 'row'
        if ~[sz(1)<sz(2)]
            vout=v';
        else
            vout=v;
        end
    case 'column'
        if ~[sz(1)>sz(2)]
            vout=v';
        else
            vout=v;
        end
    otherwise
            error('Input ''rc'' not recognized. Options are: ''row'' and ''column''.')
end


end

%% Internal functions specific to splitanalysis
%% 
function [Model,Iter,Err,ttc] = trackprogress(futures,numstarts,toolbox,splitsource,factors)
% Monitor parfeval progress supervision (c) Urban W�nsch, 2016-2019
% This one is splitanalysis.m-specific.

% Allocation of PARAFAC outputs
Model = cell(numstarts,1);           % Allocate space for all results
Err   = cell(numstarts,1);           % Allocate space for all results
Iter  = cell(numstarts,1);           % Allocate space for all results

% Allocation of monitoring variables
fetchthese=true(1,numel(numstarts));% fetchNext: only these (not cancelled)
completed    = false(numstarts,1);     % Complete / incomplete?

numCompleted = 0;                  % How many simulations have completed
ttc=repmat(duration,numstarts,1);


% Allocation of diary variables (aka console output in parfeval)
diariesOld    = cell(numstarts);
output     = cell(numstarts);
diaries    = cell(numstarts);


% 1st Blockbar
state=false(numel(unique(factors)),numstarts/numel(unique(factors)));
mname=cellstr(strcat(repmat('Model',numel(unique(factors)),1),...
    num2str(unique(factors)),...
    repmat(' (col = split #)',numel(unique(factors)),1)))';
col=lines(numel(unique(splitsource)));
col=repelem(col,numstarts/numel(unique(factors))./numel(unique(splitsource)),1);

% Close any old blockbar
figHandles = get(0,'Children');
for n=1:numel(figHandles)
    if strcmp(figHandles(n).Name,'Progress...')
        close(figHandles(n));
    end
end
clearvars n figHandles
blockbar(mname,state,col);

while numCompleted < numstarts
    ffetch=futures(fetchthese&~completed');
    [completedIdx,Modelres] = fetchNext(ffetch,0.1);
    % If fetchNext returned an output, let's extract it.
    if ~isempty(completedIdx)
        % Get actual index of future if ffetch-futures were subset
        [~,completedIdx]=intersect([futures.ID],ffetch(completedIdx).ID);
        numCompleted = numCompleted + 1;
        % Update list of completed futures.
        completed(completedIdx) = true;
        % Update state to account for completed futures
        state=reshape(completed,numstarts/numel(unique(factors)),numel(unique(factors)))';
        Model{completedIdx,1} = rcvec(Modelres.loads,'row');
        Iter{completedIdx,1} = Modelres.detail.critfinal(3);
        Err{completedIdx,1} = Modelres.detail.ssq.residual;
        
        ttc(completedIdx)=datetime(futures(completedIdx).FinishDateTime,'TimeZone','Europe/London')-...
            datetime(futures(completedIdx).StartDateTime,'TimeZone','Europe/London');

        disp(['mod-id ',sprintf('%03d',completedIdx),' done.']);
        else
        % Console output of parfeval diary.
        for i=1:numel(futures)
            if strcmp(futures(i).State,'running')
                output{i}=futures(i).Diary;
                diaries{i}=strsplit(output{i},'\n');
                [diariesOld{i}]=printupdates(diaries{i},diariesOld{i},i,toolbox);
            end
        end
    end
    % Check status of blockbar
    c=blockbar(mname,state,col);
    
    % Check if any models were cancelled and cancel the unfinished jobs among these models
    fetchthese=~repelem(c,numstarts/numel(unique(factors)),1)';
    if any(~fetchthese)
        if ~isempty(futures(~fetchthese&~completed'))
            cancel(futures(~fetchthese&~completed'));
            Err(~fetchthese&~completed',1)=num2cell(nan(sum(~fetchthese&~completed'),1));
            numCompleted=numCompleted+numel(futures(~fetchthese&~completed'));
            completed(~fetchthese) = true;
            warning('User canceled some models prematurely. No output fetched for those.')
        end
    end
    
end

% cancel the futures and delete the waitbar.
cancel(futures);
blockbar('close');

end

%%
function [cncl] = blockbar(names,state,col)
% Blockbar is similar to a multiwaitbar, but visualizes the state of
% individual jobs rather than a % completed bar.
% (c) Urban W�nsch, 2019
%% Default options
icalpha = 0.2;
 calpha = 1;

multi=numel(names);
init=true; % init state is true by default. When old figure is found, init = false.
cncl=false(numel(names),1);
%% Find existing blockbar and axes within it (not the buttons)
figHandles =  get(groot, 'Children');
for n=1:numel(figHandles)
    if strcmp(figHandles(n).Name,'Progress...')
        init=false; % Success, do not make another figure, use the old
        old=figHandles(n);
        break
    end
end
% Close blockbar if 'close' is requested by user
if strcmp(class(names),'char')&&sum(strcmp(names,'close'))==1
    try
       close(old)
       return
    catch
        warning('Coudn''t close the blockbar. Probably, there was none.')
        return
    end
end
% Check input
if numel(names)~=size(state,1)
    warning('Size of ''names'' and ''state'' are inconsistent. Can''t draw the blockbar...')
    return
end
if size(state,2)~=size(col,1)
    warning('Size of ''state'' and ''col'' are inconsistent. Can''t draw the blockbar...')
    return
end
%% Draw the blockbar
stp=[0:1/size(state,2):1 1];
if init % This generates the figure and bars
    set(0, 'Units', 'pixel');
    screenSize = get(0,'ScreenSize');
    pointsPerPixel = 72/get(0,'ScreenPixelsPerInch');
    width = 360 * pointsPerPixel;
    height = multi * 75 * pointsPerPixel;
    fpos = [screenSize(3)/2-width/2 screenSize(4)/2-height/2 width height];
    figureh=figure('Position',fpos,...
        'MenuBar','none',...
        'Numbertitle','off',...
        'Name','Progress...',...
        'Resize','off');
    axeswidth = 172;
    axesheight = 172/14;
    axesbottom = fliplr(linspace(15,fpos(4)-axesheight*3,multi));
    addprop(figureh,'state');
    addprop(figureh,'sname');
    figureh.state=state;
    figureh.sname=names;
    for i=1:numel(names)
        axPos = [axesheight*2.5 axesbottom(i) axeswidth axesheight];
        bPos = [axPos(1)+axPos(3)+10 axPos(2) 50 axPos(4)*1.5];
        bPos(2) = bPos(2)-0.5*(bPos(4)-axPos(4));
        ax(i)=axes('units','pixel','pos',axPos);
        addprop(ax(i),'sname');
        set(ax(i),'units','pixel','Tag',names{i});
        uic=uicontrol( 'Style', 'togglebutton', ...
            'String', '', ...
            'FontWeight', 'Bold', ...
            'units','pixel',...
            'Position',bPos,...
            'Tag',num2str(i),...
            'String','Cancel','FontWeight','normal');
        addprop(uic,'snames');
        uic.snames=i;
        
        title(ax(i),names{i});
        set(ax(i),'YTickLabel','');
        set(ax(i),'XTickLabel','');
        for n=1:size(state,2)
            if state(i,n)
                av=calpha;
            else
                av=icalpha;
            end
            p=patch(ax(i),[stp(n) stp(n+1) stp(n+1) stp(n)],[0 0 1 1],col(n,:),...
                'EdgeColor','none','FaceAlpha',av);hold on
            addprop(p,'id');
            p.id=n;
        end
        set(ax(i),'YColor',[0 0 0 0.5],'XColor',[0 0 0 0.5],'Box','on','YTick','','XTick','')
    end
else % This updates  the figure and bars
    oldstate=old.state;
    if ~isequal(oldstate,state)
        ax=get(old,'Children');
        ax=ax(strcmp(get(ax,'Type'),'axes'));
        ni=multi:-1:1;
        for n=1:numel(names)
            newstate=state(n,:);
            if ~isequal(oldstate,newstate)
                h=get(ax(ni(n)),'Children');
                ii=size(state,2):-1:1;
                for i=1:size(state,2)
                    if state(n,i)&&~oldstate(n,i)
                        h(ii(i)).FaceAlpha=calpha;
                    end
                end
            end
        end
        old.state=state;
    end
    childs=get(old,'Children');
    tbutt=childs(strcmp(get(childs,'Type'),'uicontrol'));
    ni=multi:-1:1;    
    for n=1:numel(names)
        if logical(tbutt(ni(n)).Value)&any(~state(n,:))
            cncl(n)=true;
        else
            cncl(n)=false;
        end
    end
    
end
end