function eemreview(ds,varargin)
%
% <strong>Syntax</strong>
%
%   <strong>eemreview</strong>(data,Name,Value)
% <a href="matlab: doc eemreview">help for eemreview</a> <- click on the link

% Notice:
% This mfile is part of the drEEM toolbox. Please cite the toolbox
% as follows:
%
% Murphy K.R., Stedmon C.A., Graeber D. and R. Bro, Fluorescence
%     spectroscopy and multi-way techniques. PARAFAC, Anal. Methods, 2013,
%     DOI:10.1039/c3ay41160e.
%
% eemreview: Copyright (C) 2021 Urban J. Wünsch
% Technical University of Denmark
% Kemitorvet
% DK-2800 Kgs. Lyngby
% urbw@dtu.dk

%% Set options
params = inputParser;
params.addParameter('eem', 'X', @ischar);
params.addParameter('title', 'filelist', @ischar);
params.addParameter('samples', [], @isnumeric);
params.addParameter('nContours', 15, @isnumeric);
params.addParameter('LineStyle', '-', @ischar);
params.addParameter('peaks', false, @islogical);
params.addParameter('hold', false, @islogical);
params.addParameter('keepselection',false, @islogical);
params.addParameter('showabsorbance',false,@islogical);
params.parse(varargin{:});

opts.Xname      = params.Results.eem;
opts.ContLine   = params.Results.LineStyle;
opts.titlefield = params.Results.title;
opts.ncontours  = params.Results.nContours;
opts.peakSwitch = params.Results.peaks;
opts.holdSwitch = params.Results.hold;
opts.keepSwitch = params.Results.keepselection;
opts.showabs    = params.Results.showabsorbance;
opts.sample     = 1;
opts.spectra    = [];


if opts.peakSwitch
    opts.ContLine = 'none';
end

if opts.ncontours >= 50
    opts.ContLine = 'none';
end

opts.offerabs=false;
if isfield(ds,'Abs_wave')&&isfield(ds,'Abs')
    if size(ds.Abs_wave,1)==size(ds.Abs,2)
        if size(ds.Abs,1)==ds.nSample
            opts.offerabs=true;
        end
    end
end

%% Prepare ds for the plotting job
f=findcomponents(ds);
opts.f=f;
for n=1:numel(f)
    ds.(['X_Model',num2str(f(n))])=nmodel(ds.(['Model',num2str(f(n))]));
    ds.(['X_Model',num2str(f(n)),'resi'])=ds.X-ds.(['X_Model',num2str(f(n))]);
end

if not(contains(opts.Xname,'resi'))&&not(contains(opts.Xname,'Model'))
    opts.eemmode='data';
    opts.Xtype='data';
elseif contains(opts.Xname,'Model')&&not(contains(opts.Xname,'resi'))
    opts.eemmode='data';
    opts.Xtype='model';
elseif contains(opts.Xname,'resi')
    opts.eemmode='residuals';

    opts.Xtype='residual';
end


try
    filelist=ds.(opts.titlefield);
    if isnumeric(filelist)
        filelist=cellstr(num2str(filelist));
    end
catch
    warning('Could not parse the info for plot titles. Using simple indicies instead. ')
    filelist=cellstr(num2str((1:ds.nSample)'));
end

for n=1:numel(filelist)
    filelist{n}=[num2str(n),' : ',filelist{n}];
    try %#ok<TRYNC>
        filelist{n}=[filelist{n},' (.i ',num2str(ds.i(n)),')'];
    end
end
ds.filelist=filelist;

%% Figure setup
figHandles = get(0,'Children');
for n=1:numel(figHandles);if strfind(figHandles(n).Name,'eemreview');close(figHandles(n));end;end
clearvars figHandles
figureh=dreemfig;

addprop(figureh,'opts');
addprop(figureh,'data');
figureh.opts=opts;
figureh.data=ds;
figureh.MenuBar='none' ;
figureh.ToolBar='figure';
set(figureh, 'units','normalized','pos',[0.2453    0.1611    0.5448    0.5000]);
if ~isfield(ds,'name')
    set(figureh,'Name','eemreview')
else
    try set(figureh,'Name',['eemreview - dataset:',ds.name{1}])
    catch
        set(figureh,'Name',['eemreview - dataset:',ds.name])
    end
end

panel(1) = uipanel(figureh,'Position',[0.001 0.25    0.60    0.8 ],...
    'BackgroundColor','w','BorderType','none'); % EEM axis
panel(2) = uipanel(figureh,'Position',[0.61  0.02    0.39    0.95],...
    'BackgroundColor','w','BorderType','none'); % 2D figures
panel(3) = uipanel(figureh,'Position',[0.001 0.0200  0.3     0.2],...
    'BackgroundColor','w','BorderType','none'); % Sample navigation
panel(4) = uipanel(figureh,'Position',[0.45   0.0200  0.15     0.15],...
    'BackgroundColor','w','BorderType','none'); % Spectra button
panel(5) = uipanel(figureh,'Position',[0.32   0.0200  0.13     0.15],...
    'BackgroundColor','w','BorderType','none'); % Control toggles

ax(1) = axes(panel(1),'pos',[0.15    0.1    0.6    0.75]);
ax(2) = axes(panel(2),'pos',[0.15    0.2    0.75    0.3]);
ax(3) = axes(panel(2),'pos',[0.15    0.65    0.75    0.3]);
ax(2).Title.String='Ex.spectra';
ax(1).Tag='eem';
ax(2).Tag='em';
ax(3).Tag='ex';
addprop(ax(2),'keepplotting');
addprop(ax(3),'keepplotting');
for j=1:3 axtoolbar(ax(j));end
ax(2).Title.String='Emission';
ax(3).Title.String='Excitation';

for n=1:3;set(ax(n),'Box','on');end

% Define uicontrol elements
positions=[0.2,0.35,0.75,0.3;... % popupmenu 1
    0.02,0.01,0.9,0.05;...       % RayRam text field 2
    0.2,0.05,0.3,0.3;...          % Previous 3 
    0.1,0.4,0.8,0.4;...          % Spectra 4
    0.55,0.05,0.4,0.3;...         % Next 5
    0.9157 0.1 0.04 0.3;...  % CLim slider (1) lower lim 6 
    0.05 0.8 0.9 0.2;...  % Checkbox 1 (Show abs) 7
    0.05 0.55 0.9 0.2;...  % Checkbox 2 (Show peaks) 8
    0.05 0.3 0.9 0.2;...  % Checkbox 3  9
    0.05 0.05 0.9 0.2;...  % Checkbox 4 (show abs) 10
    0.9157 0.55 0.04 0.3;...  % CLim slider (2) upper lim 11
    0.2 0.65 0.3 0.3; % Popupmenu (which data) 12
    0.65 0.65 0.3 0.3;  % Popupmenu (which model) 13
    ];
% Popup menu
plt_h(1)=uicontrol(panel(3),...
    'Style','popupmenu',...
    'Position',[100 20 100 20],...
    'Tag','popup',...
    'String',ds.filelist,...
    'Units','normalized',...
    'TooltipString', 'Jump to any sample in the dataset.',...
    'Position', positions(1,:));
Xnameopts={'data','model','residual'};
plt_h(5)=uicontrol(panel(3),...
    'Style','popupmenu',...
    'Position',[100 20 100 20],...
    'Tag','popup_type',...
    'String',Xnameopts,...
    'Value',find(matches(Xnameopts,opts.Xtype)),...
    'Units','normalized',...
    'TooltipString', 'Type of data?',...
    'Position', positions(12,:));

plt_h(6)=uicontrol(panel(3),...
    'Style','popupmenu',...
    'Position',[100 20 100 20],...
    'Tag','popup_model',...
    'String',cellstr(strcat(num2str(f'),repmat(' - component model',numel(f),1))),...
    'Units','normalized',...
    'TooltipString', 'Which model (unavailable when "data" is selected)',...
    'Position', positions(13,:));
if isempty(f)
    plt_h(5).Enable='off';
    plt_h(6).Enable='off';
end
if matches(opts.Xtype,'data')
    plt_h(6).Enable='off';
end
% Previous sample
plt_h(2) = uicontrol(panel(3),...
    'Style', 'pushbutton',...
    'Tag','prev',...
    'Enable','off',...
    'String','Prev.',...
    'Units','normalized',...
    'TooltipString', 'Plot the previous sample in the dataset.',...
    'Position', positions(3,:));
% Next sample
plt_h(3) = uicontrol(panel(3),...
    'Style', 'pushbutton',...
    'Tag','next',...
    'String','Next',...
    'FontWeight','bold',...
    'Units','normalized',...
    'TooltipString', 'Plot the next sample in the dataset.',...
    'Position', positions(5,:));
% Plot spectra
plt_h(4) = uicontrol(panel(4),...
    'Style', 'pushbutton',...
    'String','Spectra',...
    'FontWeight','bold',...
    'Units','normalized',...
    'TooltipString','Graphically select which wavelength should be plotted in 2D.',...
    'Position', positions(4,:));

uicontrol(panel(2),...
    'Style','text',...
    'Position',[100 20 100 20],...
    'String','Black: Raman, red: Rayleigh scatter ',...
    'Units','normalized',...
    'Position', positions(2,:),...
    'BackgroundColor',[1 1 1],...
    'HorizontalAlignment','center');

lay_h(1) = uicontrol(panel(1),...
    'Style', 'slider',...
    'String','CLim lower',...
    'Tag','CLim lower',...
    'Min',0,'Max',1,'Value',0.5,...
    'SliderStep', [0.001,0.2], ...
    'Units','normalized',...
    'Position', positions(6,:));
lay_h(2) = uicontrol(panel(1),...
    'Style', 'slider',...
    'String','CLim upper',...
    'Tag','CLim upper',...
    'Min',0,'Max',1,'Value',0.5,...
    'SliderStep', [0.001,0.2], ...
    'Units','normalized',...
    'Position', positions(11,:));

% lay_h = com.jidesoft.swing.RangeSlider(0,100,20,70);  % min,max,low,high
% lay_h = javacomponent(lay_h, [0,0,200,80], panel(1));
% set(lay_h, 'MajorTickSpacing',25, 'MinorTickSpacing',5, 'PaintTicks',true, 'PaintLabels',true, ...
%     'Background',java.awt.Color.white, 'StateChangedCallback',@setclim);
% lay_h.Orientation=1;

cb_h(1) = uicontrol(panel(5),...
    'Style', 'checkbox',...
    'String','hold spectra',...
    'Tag','hold',...
    'Min',false,'Max',true,'Value',opts.holdSwitch,...
    'Units','normalized',...
    'BackgroundColor',[1 1 1],...
    'TooltipString', 'If checked, multiple 2D spectra can be overlayed. Otherwise, plots will be overwritten.',...
    'Position', positions(9,:));
cb_h(2) = uicontrol(panel(5),...
    'Style', 'checkbox',...
    'String','show peaks',...
    'Tag','peaks',...
    'Min',false,'Max',true,'Value',opts.peakSwitch,...
    'Units','normalized',...
    'BackgroundColor',[1 1 1],...
    'TooltipString', 'If checked, the reference positions of classic FDOM "peaks" is overlayed.',...
    'Position', positions(8,:));
cb_h(3) = uicontrol(panel(5),...
    'Style', 'checkbox',...
    'String','keep selection',...
    'Tag','keepsel',...
    'Min',false,'Max',true,'Value',opts.keepSwitch,...
    'Units','normalized',...
    'BackgroundColor',[1 1 1],...
    'TooltipString', 'If checked, the last ''Spectra'' wavelength selection will be retained between samples.',...
    'Position', positions(10,:));

cb_h(4) = uicontrol(panel(5),...
    'Style', 'checkbox',...
    'String','show absorbance',...
    'Tag','abs',...
    'Min',false,'Max',true,'Value',opts.showabs,...
    'Units','normalized',...
    'BackgroundColor',[1 1 1],...
    'Position', positions(7,:),...
    'TooltipString', ['If checked, the absorbance will be plotted in the excitation plot.',...
    'If gray''ed out, either no absorbance present or it is not formatted correctly.'],...
    'Visible','on');


if opts.offerabs
    cb_h(4).Enable='on';
else
    cb_h(4).Enable='off';
end
tmp_h = findall(figureh,'type','uicontrol');
for n=1:numel(tmp_h)
    tmp_h(n).FontSize=9;
    tmp_h(n).FontName='Arial';
end
clearvars tmp_h


if matches(figureh.opts.eemmode,'residuals')
    set(lay_h(1),'Visible','off')
    set(lay_h(2),'Visible','off')
end


%% Set the callbacks
set(plt_h,'Callback',{@newsample})
set(plt_h(4),'Callback',{@setspecwave});
set(cb_h,'Callback',{@check2opt});
set(lay_h,'Callback',{@setclim});

initiateplot

end
%% Internal functions we will need

function newsample(src,~)
fig = get(0,'Children');
fig = fig(arrayfun(@(x) contains(x.Name,'eemreview'),fig));
hmodelsel=findinfigure('uicontrol','popup_model');
hsamplesel=findinfigure('uicontrol','popup');
hCLim(1)=findinfigure('uicontrol','CLim lower');
hCLim(2)=findinfigure('uicontrol','CLim upper');

if any(matches(src.String,'Next'))
    fig.opts.sample=fig.opts.sample+1;
    hsamplesel.Value=fig.opts.sample;
elseif any(matches(src.String,'Prev.'))
    fig.opts.sample=fig.opts.sample-1;
    hsamplesel.Value=fig.opts.sample;
elseif matches(src.Tag,'popup')
    fig.opts.sample=src.Value;
elseif matches(src.Tag,'popup_type')
    fig.opts.Xtype=src.String{src.Value};
    if matches(fig.opts.Xtype,'data')
        fig.opts.Xname='X';
    end
    switch src.String{src.Value}
        case 'data'
            fig.opts.eemmode='data';
            hmodelsel.Enable='off';
            hCLim(1).Enable='on';
            hCLim(2).Enable='on';
        case 'model'
            fig.opts.eemmode='data';
            hmodelsel.Enable='on';
            hCLim(1).Enable='on';
            hCLim(2).Enable='on';
        case 'residual'
            fig.opts.eemmode='residuals';
            hmodelsel.Enable='on';
            
            hCLim(1).Enable='off';
            hCLim(2).Enable='off';
    end
        
elseif matches(src.Tag,'popup_model')
    fig.opts.Xname=src.String{src.Value};
end
hp=findinfigure('uicontrol','prev');
hn=findinfigure('uicontrol','next');

if fig.opts.sample<=1
    hp.Enable='off';
else
    hp.Enable='on';
end

if fig.opts.sample>=fig.data.nSample
    hn.Enable='off';
else
    hn.Enable='on';
end


cl={'ex','em'};tit={'Excitation','Emission'};
for n=1:2
    h=findinfigure('axes',cl{n});
    cla(h)
    legend(h,'off')
    title(h,tit{n})
end


if fig.opts.keepSwitch&&not(isempty(fig.opts.spectra))
    specview
end

initiateplot



end

function check2opt(src,~)
fig = get(0,'Children');
fig = fig(arrayfun(@(x) contains(x.Name,'eemreview'),fig));

if     matches(src.Tag,'hold')
    fig.opts.holdSwitch=src.Value;
    
elseif matches(src.Tag,'peaks')
    fig.opts.peakSwitch=src.Value;
    overlaypeaks
elseif matches(src.Tag,'keepsel')
    fig.opts.keepSwitch=src.Value;
    if fig.opts.keepSwitch&&isempty(fig.opts.spectra)
        setspecwave
    end
    
    if not(fig.opts.keepSwitch)
        fig.opts.spectra=[];
    end
    
elseif matches(src.Tag,'abs')
    fig.opts.showabs=src.Value;
    plotabsorbance
end
end

function setspecwave(~,~)
fig = get(0,'Children');
fig = fig(arrayfun(@(x) contains(x.Name,'eemreview'),fig));
mindist=@(vec,val) find(ismember(abs(vec-val),min(abs(vec-val))));
data=fig.data;
[x,y]=ginput(1);


fig.opts.spectra=[data.Em(mindist(data.Em,y)) data.Ex(mindist(data.Ex,x))];
specview

end

function setclim(~,~)
fig = get(0,'Children');
fig = fig(arrayfun(@(x) contains(x.Name,'eemreview'),fig));
ax=findinfigure('axes','eem');

ConLim=get(ax,'CLim');


sl_l=findinfigure('uicontrol','CLim lower');
sl_u=findinfigure('uicontrol','CLim upper');


lower = sl_l.Value;
upper = sl_u.Value;
newCLim=[lower upper];
plottype='new';
switch fig.opts.eemmode
    case 'data'
        try
            set(ax,'CLim',newCLim);
        catch
            set(ax,'CLim',[ConLim(1) ConLim(2)]);
        end
    case 'residuals'
        switch plottype
            case 'old'
                h=ax.Children;
                h=h(2);
                levels=linspace(newCLim(1),newCLim(2),numel(h.LevelList));
                n_neg=sum(levels<0);
                n_pos=sum(levels>0);
                colormap(ax(1),residualcolormap(n_neg,n_pos))
            case 'new'
                % Won't work with this plot type currently
%                 h=ax(1).Children;
%                 h=h(2);
%                 levels=linspace(newCLim(1),newCLim(2),100);
%                 n_neg=sum(levels<0);
%                 n_pos=sum(levels>0);
%                 colormap(ax(1),residualcolormap(n_neg,n_pos))
        end
end
end

function h = findinfigure(element,name)
fig = get(0,'Children');
fig = fig(arrayfun(@(x) contains(x.Name,'eemreview'),fig));
eleh = findall(fig,'type',element);
h = findall(eleh,'Tag',name);
if isempty(h)
    disp(eleh)
    error('Error: Could not find one of the graphical elements with the expected name')
end
end

function cols = colmaker(i)
collist= [0 0 0;...
    0.368627450980392,0.309803921568627,0.635294117647059;...
    0.197386603025884,0.512851710442123,0.740257214831838;...
    0.353920722469962,0.729472517932409,0.656177840632872;...
    0.597784406620190,0.840777034550949,0.644490274269565;...
    0.841701969675001,0.940885130292891,0.609611616709332;...
    0.931851609124017,0.957193786605551,0.661676168589149;...
    0.976267475434952,0.928364732010819,0.658313136694680;...
    0.995493878994122,0.822714483114813,0.482765825462557;...
    0.987654277338283,0.615414278604657,0.339114037801447;...
    0.943815034723900,0.390980258870390,0.266846392952373;...
    0.820562112287474,0.223926402795306,0.309367864350849;...
    0.619607843137255,0.00392156862745098,0.258823529411765];
cols=collist(1:i,:);
end

function f=findcomponents(model)
cnt=1;
f=zeros(1,100);
for n=1:100
    if isfield(model,['Model',num2str(n)])
        f(cnt)=n;
    end
    cnt=cnt+1;
end
f(f==0)=[];
end

function initiateplot

fig = get(0,'Children');
fig = fig(arrayfun(@(x) contains(x.Name,'eemreview'),fig));
opts=fig.opts;
sample = opts.sample;
data=fig.data;
ax=findinfigure('axes','eem');

modelh=findinfigure('uicontrol','popup_model');
if not(isempty(opts.f))
    fhere=opts.f(modelh.Value);
end
if contains(opts.Xtype,'resi')
    Xname=['X_Model',num2str(fhere),'resi'];
    try
        eem=squeeze(data.(Xname)(sample,:,:));
    catch
        error('Could not retreive residuals for plotting. Run "checkdataset".')
    end
elseif contains(opts.Xtype,'model')
    Xname=['X_Model',num2str(fhere)];
    try
        eem=squeeze(data.(Xname)(sample,:,:));
    catch
        error('Could not retreive model for plotting. Run "checkdataset".')
    end
else
    eem=squeeze(data.(opts.Xname)(sample,:,:));
end
% Plotting
eemcontourf(ax,...
    data.Ex,data.Em,eem,...
    sample,opts.ncontours,opts.ContLine,opts.eemmode)

ConLim=get(ax,'CLim');
try
    xhere=squeeze(data.(opts.Xname)(fig.opts.sample,:,:));xhere=xhere(not(isnan(xhere(:))));
    maxval=max(xhere);
    minval=min(xhere); clearvars xhere
    if minval==0||minval>0
        l_lims = [-maxval*2 .005*maxval];
    elseif minval<0
        l_lims = [2*minval .005*maxval];
    end
    
    u_lims = [.0051*maxval 4*maxval];
    sl_l=findinfigure('uicontrol','CLim lower');
    sl_u=findinfigure('uicontrol','CLim upper');
    
    set(sl_l,'Min',l_lims(1),'Max',l_lims(2),'Value',ConLim(1))
    set(sl_u,'Min',u_lims(1),'Max',u_lims(2),'Value',ConLim(2))
catch
end



% Formatting
if isempty(findall(gcf,'type','ColorBar'))
    c=colorbar(ax);
    c.Position=[0.78 0.1 0.0470 0.75];
end
xlabel(ax,'Excitation (nm)');ylabel(ax,'Emission (nm)');
title(ax,[num2str(sample),'/',num2str(data.nSample),': ',char(data.(opts.titlefield)(sample))], 'Interpreter', 'none');

if opts.peakSwitch
    overlaypeaks
end
if opts.offerabs&&opts.showabs
   plotabsorbance
end
end

function eemcontourf(ax,x,y,mat,i,ncontours,ContLine,eemtype)

plottype='new'; % Change this to change the flavor of the plotting.

if numel(unique(mat))==1
    cla(ax)
    plot(ax(1),NaN,NaN,'DisplayName',num2str(i));
    text(ax(1),290,400,'Cannot plot constant data.')
    xlim(ax(1),[250 350])
    ylim(ax(1),[350 450])
    return
end
hold(ax,'off')
switch eemtype
    case 'data'
        switch plottype
            case 'old'
                contourf(ax,x,y,mat,100,'Color',[.3 .3 .3],'LineStyle','none','DisplayName',num2str(i));
                hold(ax,'on')
                contour(ax,x,y,mat,ncontours,'Color','k','LineStyle',ContLine);
                caxis(ax,[min(mat(~isnan(mat))) max(mat(~isnan(mat)))])
            case 'new'
                sc = surfc(ax,x,y,mat,'EdgeColor','none');
                view(ax,0,90)
                if isempty(max(mat(~isnan(mat))))
                    return
                else
                    ax.ZLim(2) = max(mat(~isnan(mat)));
                end
                sc(2).ZLocation = 'zmax';
                sc(2).LineColor='k';
                sc(2).LevelList=linspace(min(mat(~isnan(mat))),max(mat(~isnan(mat))),ncontours);
                try
                    cmap=turbo;
                    %cmap(1,:)=[1 1 1];
                    colormap(ax,cmap)
                catch
                    colormap(ax,parula)
                end
        end
    case 'residuals'
        switch plottype
            case 'old'
                contourf(ax,x,y,mat,200,'Color',[.3 .3 .3],'LineStyle','none','DisplayName',num2str(i));
                hold(ax,'on')
                contour(ax,x,y,mat,ncontours,'Color','k','LineStyle',ContLine);
                CLim=[min(mat(~isnan(mat))) max(mat(~isnan(mat)))];
                caxis(ax,CLim)

                h=ax.Children;
                h=h(2);
                levels=linspace(CLim(1),CLim(2),numel(h.LevelList));
                n_neg=sum(levels<0);
                n_pos=sum(levels>0);
                colormap(ax,residualcolormap(n_neg,n_pos))
            case 'new'
                
                sc = surfc(ax,x,y,mat,'EdgeColor','none');
                view(ax,0,90)
                ax.ZLim(2) = max(mat(~isnan(mat)));
                sc(2).ZLocation = 'zmax';
                sc(2).LineColor='k';
                sc(2).LevelList=linspace(min(mat(~isnan(mat))),max(mat(~isnan(mat))),ncontours);

                CLim=[min(mat(~isnan(mat))) max(mat(~isnan(mat)))];
                caxis(ax,CLim)

                levels=linspace(CLim(1),CLim(2),100);
                n_neg=sum(levels<0);
                n_pos=sum(levels>0);
                colormap(ax,residualcolormap(n_neg,n_pos))
                
        end
end
ax.Tag='eem'; % needs to be set again for some reason.
end

function specview
fig = get(0,'Children');
fig = fig(arrayfun(@(x) contains(x.Name,'eemreview'),fig));
opts=fig.opts;
data=fig.data;

if isempty(opts.spectra)
    return
end


modelh=findinfigure('uicontrol','popup_model');
if not(isempty(opts.f))
    fhere=opts.f(modelh.Value);
end
if contains(opts.Xtype,'resi')
    Xname=['X_Model',num2str(fhere),'resi'];
    try
        eem=squeeze(data.(Xname)(opts.sample,:,:));
    catch
        error('Could not retreive residuals for plotting. Run "checkdataset".')
    end
elseif contains(opts.Xtype,'model')
    Xname=['X_Model',num2str(fhere)];
    try
        eem=squeeze(data.(Xname)(opts.sample,:,:));
    catch
        error('Could not retreive model for plotting. Run "checkdataset".')
    end
else
    eem=squeeze(data.('X')(opts.sample,:,:));
end


ax = findinfigure('axes','ex');


hc=get(ax,'Children');
if opts.holdSwitch
    for n=1:numel(hc)
        if ~isempty(strfind(hc(n).DisplayName,'Raman'))||~isempty(strfind(hc(n).DisplayName,'Rayleigh'))
            %contains(hc(n).DisplayName,{'Raman','Rayleigh'}) % If only everyone would use the latest software
            delete(hc(n))
        end
    end
end

if opts.holdSwitch
    hold(ax,'on')
else 
    hold(ax,'off')
end



plot(ax,data.Ex,squeeze(eem(data.Em==opts.spectra(1),:)),'-k','DisplayName',num2str(round(opts.spectra(1))))

h=findobj(ax,'Type', 'line');
h=h(not(cellfun(@(x) isempty(x),(arrayfun(@(x) x.DisplayName,h,'uni',false)))));
if numel(h)>1
    col=colmaker(numel(h));
    set(h, {'color'}, num2cell(col,2));
    legend1 = legend(h,'location','best');
    try %#ok<TRYNC>
        legend1.ItemTokenSize=[10 6];
    end
end


cylim=get(ax,'YLim');
cxlim=get(ax,'XLim');

y = [0 cylim(2)];
wl = opts.spectra(1);
ray1st=[wl wl];
ray2nd=ray1st/2;

ram1=(1*10^7*((1*10^7)/(wl)+3382)^-1);
ram2=(1*10^7*((1*10^7)/(wl/2)+3382)^-1);

ram1st=[ram1 ram1];
ram2nd=[ram2 ram2];
if not(opts.holdSwitch)
    line(ax,ram1st,y,...
        'DisplayName','Raman 1st','Color','k','LineWidth',0.5);
    line(ax,ram2nd,y,...
        'DisplayName','Raman 2nd','Color','k','LineWidth',0.5);
    line(ax,ray1st,y,...
        'DisplayName','Rayleigh 1st','Color','r','LineWidth',0.5);
    line(ax,ray2nd,y,...
        'DisplayName','Rayleigh 2nd','Color','r','LineWidth',0.5);
    set(ax,'XLim',cxlim);
    set(ax,'YLim',cylim);
end


set(ax,'Tag','ex')
title(ax,['Ex. spectrum at Em = ',num2str(opts.spectra(1))])


ax = findinfigure('axes','em');
hc=get(ax,'Children');
if opts.holdSwitch
    for n=1:numel(hc)
        if ~isempty(strfind(hc(n).DisplayName,'Raman'))||~isempty(strfind(hc(n).DisplayName,'Rayleigh'))
            %contains(hc(n).DisplayName,{'Raman','Rayleigh'}) % If only everyone would use the latest software
            delete(hc(n))
        end
    end
end

if opts.holdSwitch
    hold(ax,'on')
else 
    hold(ax,'off')
end
plot(ax,data.Em,squeeze(eem(:,data.Ex==opts.spectra(2))),'-k','DisplayName',num2str(round(opts.spectra(2))))


h=findobj(ax,'Type', 'line');
if numel(h)>1
    col=colmaker(numel(h));
    set(h, {'color'}, num2cell(col,2));
    legend1 = legend(h,'location','best');
    try %#ok<TRYNC>
        legend1.ItemTokenSize=[10 6];
    end
end
cylim=get(ax,'YLim');
cxlim=get(ax,'XLim');

y = [0 cylim(2)];
wl = opts.spectra(2);

ram1=1*10^7*((1*10^7)/(wl)-3382)^-1;
ram2=(1*10^7*((1*10^7)/(wl)-3382)^-1)*2;

ram1st=[ram1 ram1];
ram2nd=[ram2 ram2];
ray1st=[wl wl];
ray2nd=[wl*2 wl*2];
if not(opts.holdSwitch)
    line(ax,ram1st,y,...
        'DisplayName','Raman 1st','Color','k','LineWidth',0.5);
    line(ax,ram2nd,y,...
        'DisplayName','Raman 2nd','Color','k','LineWidth',0.5);
    line(ax,ray1st,y,...
        'DisplayName','Rayleigh 1st','Color','r','LineWidth',0.5);
    line(ax,ray2nd,y,...
        'DisplayName','Rayleigh 2nd','Color','r','LineWidth',0.5);
    set(ax,'XLim',cxlim);
    set(ax,'YLim',cylim);

end

title(ax,['Em. spectrum at Ex = ',num2str(opts.spectra(2))])
set(ax,'Tag','em')

end

function plotabsorbance
fig = get(0,'Children');
fig = fig(arrayfun(@(x) contains(x.Name,'eemreview'),fig));
opts=fig.opts;
data=fig.data;
ax=findinfigure('axes','ex');

if opts.showabs

    yyaxis(ax,'right')
    hold(ax,'off')
    plot(ax,data.Abs_wave,data.Abs(opts.sample,:),'LineStyle','-','Color',[1 .4 .4],'Marker','none')
    set(ax,'YColor',[1 .4 .4])
    axis(ax,'tight')
    ax.Title.String='Ex. / abs. spectra';
    xlabel(ax,'Ex / abs. wavelength (nm)')
    ylabel(ax,'Absorbance')
else
    yyaxis(ax,'right')
    plot(ax,nan,nan,'LineStyle','-','Color','r','Marker','none')
    set(ax,'YColor','k','YTick','')
    ax.Title.String='Excitation';
end
yyaxis(ax,'left')
set(ax,'YColor','k')
set(ax,'Tag','ex');
end

function overlaypeaks
fig = get(0,'Children');
fig = fig(arrayfun(@(x) contains(x.Name,'eemreview'),fig));
opts=fig.opts;
ax=findinfigure('axes','eem');
data=fig.data;
plottype='new';

% Formatting
lw=1.5;
OnOff=opts.peakSwitch;
col=[0 0 0];

marker='+';
markersize=20;
% Definition of peaks
peaks(1).name=cellstr('B');
peaks(1).Em=num2cell(305);
peaks(1).Ex=num2cell(275);
peaks(2).name=cellstr('T');
peaks(2).Em=num2cell(340);
peaks(2).Ex=num2cell(275);
peaks(3).name=cellstr('A');
peaks(3).Em=num2cell(400:460);
peaks(3).Ex=num2cell(260);
peaks(4).name=cellstr('M');
peaks(4).Em=num2cell(370:410);
peaks(4).Ex=num2cell(290:310);
peaks(5).name=cellstr('C');
peaks(5).Em=num2cell(420:460);
peaks(5).Ex=num2cell(320:360);
peaks(6).name=cellstr('D');
peaks(6).Em=num2cell(509);
peaks(6).Ex=num2cell(390);
peaks(7).name=cellstr('N');
peaks(7).Em=num2cell(370);
peaks(7).Ex=num2cell(280);
if OnOff
    hold(ax,'on')
    for nPeak=1:size(peaks,2)
        if numel(peaks(nPeak).Em)==1&&numel(peaks(nPeak).Ex)==1
            Labels = peaks(nPeak).name;    %'
            switch plottype
                case 'old'
                    plot(ax,peaks(nPeak).Ex{1},peaks(nPeak).Em{1},marker,'MarkerSize',markersize,'LineWidth',lw,'Color',col,'DisplayName','PeaksOverlayed')
                    text(ax,peaks(nPeak).Ex{1}+0.1,peaks(nPeak).Em{1}+0.1, Labels, 'horizontal','left', 'vertical','bottom','Color',col,'DisplayName','PeaksOverlayed');
                case 'new'
                    plot3(ax,peaks(nPeak).Ex{1},peaks(nPeak).Em{1},ax.ZLim(2),marker,'MarkerSize',markersize,'LineWidth',lw,'Color',col,'DisplayName','PeaksOverlayed')
                    text(ax,peaks(nPeak).Ex{1}+0.1,peaks(nPeak).Em{1}+0.1,ax.ZLim(2), Labels, 'horizontal','left', 'vertical','bottom','Color',col,'DisplayName','PeaksOverlayed');
            end
            
            
        elseif numel(peaks(nPeak).Ex)==1&&numel(peaks(nPeak).Em)~=1
            tempEm=data.Em(mindist(data.Em,peaks(nPeak).Em{1}):mindist(data.Em,peaks(nPeak).Em{end}),1);
            Labels = peaks(nPeak).name;    %'
            switch plottype
                case 'old'
                    plot(ax,repmat(data.Ex(mindist(data.Ex,peaks(nPeak).Ex{1}),1),numel(tempEm)),tempEm,'LineWidth',lw,'Color',col,'DisplayName','PeaksOverlayed')
                    text(ax,data.Ex(mindist(data.Ex,peaks(nPeak).Ex{1}),1)+0.1,data.Em(mindist(data.Em,peaks(nPeak).Em{1}),1)+0.1, Labels, 'horizontal','left', 'vertical','bottom','Color',col,'DisplayName','PeaksOverlayed');
                case 'new'
                    plot3(ax,repmat(peaks(nPeak).Ex{1},numel(tempEm)),tempEm,repelem(ax.ZLim(2),numel(tempEm),1),'LineWidth',lw,'Color',col,'DisplayName','PeaksOverlayed')
                    text(ax,peaks(nPeak).Ex{1}+0.1,peaks(nPeak).Em{1}+0.1,ax.ZLim(2), Labels, 'horizontal','left', 'vertical','bottom','Color',col,'DisplayName','PeaksOverlayed');
            end
            
            
        else
            x1=peaks(nPeak).Ex{1};
            x2=peaks(nPeak).Ex{end};
            y1=peaks(nPeak).Em{1};
            y2=peaks(nPeak).Em{end};
            x = [x1, x2, x2, x1, x1];
            y = [y1, y1, y2, y2, y1];
            Labels = peaks(nPeak).name;
            switch plottype
                case 'old'
                    plot(ax,x, y, 'b-', 'LineWidth', lw,'Color',col,'DisplayName','PeaksOverlayed');
                    text(ax,data.Ex(mindist(data.Ex,peaks(nPeak).Ex{1}),1)+0.1,data.Em(mindist(data.Em,peaks(nPeak).Em{1}),1)+0.1, Labels, 'horizontal','left', 'vertical','bottom','Color',col,'DisplayName','PeaksOverlayed');
                case 'new'
                    plot3(ax,x, y , repelem(ax.ZLim(2),numel(y),1), 'b-', 'LineWidth', lw,'Color',col,'DisplayName','PeaksOverlayed');
                    text(ax,peaks(nPeak).Ex{1}+0.1,peaks(nPeak).Em{1}+0.1,ax.ZLim(2), Labels, 'horizontal','left', 'vertical','bottom','Color',col,'DisplayName','PeaksOverlayed');
            end
            
            
        end
    end
    hold(ax,'off')
else
    hc=get(ax,'Children');
    for n=1:numel(hc)
        if ~isempty(strfind(hc(n).DisplayName,'PeaksOverlayed'))||~isempty(strfind(hc(n).DisplayName,'PeaksOverlayed'))
            %contains(hc(n).DisplayName,{'Raman','Rayleigh'}) % If only everyone would use the latest software
            delete(hc(n))
        end
    end
end
end

function map = residualcolormap(nneg,npos)

% ncol=ceil(round(ncol)/2);

nmap=[linspace(0,1,nneg)' linspace(0.45,1,nneg)' linspace(0.737,1,nneg)'];
pmap=flipud([flipud(linspace(1,0.686,npos)') flipud(linspace(1,0.208,npos)') flipud(linspace(1,0.278,npos)')]);

map=[nmap;pmap];

end
