function [dataout] = associatemetadata(data,pathtofile,datakey,metadatakey)
%
% <strong>Syntax</strong>
%   [dataout] = <strong>associatemetadata</strong>(data,pathtofile,datakey,metadatakey)
%
% <a href="matlab: doc associatemetadata">help for associatemetadata</a> <- click on the link



% Catch some common mistakes in inputs
if not(isfield(data,datakey))
    error(['field: ',datakey,'does not exist in ''data.'''])
end

% Read in the metadata
if ischar(pathtofile)||isstring(pathtofile)
    warning off % Readtable will complain about Column headers a lot
    md = readtable(pathtofile);
    warning on

    if not(any(matches(md.Properties.VariableNames,metadatakey)))
        disp(md.Properties.VariableNames)
        error(['column header: ',metadatakey,'does not exist in '' imported metadata table.'''])
    end
elseif istable(pathtofile)
    md=pathtofile;
else
    error('Input to ''pathtofile'' can only be a string, character, or table.')
end
% Get the datatypes in the table
type = cell(width(md),1);
for j = 1:width(md)
    try
        type{j,1} = class(md.(md.Properties.VariableNames{j}){1});
    catch
        type{j,1} = class(md.(md.Properties.VariableNames{j})(1));
    end
    
    if matches(type{j},'cell')
        type{j}='char';
    end
    
end

% Convert types to compatible types (Quick fix for 0.6.3 subdataset function)
for j=1:numel(type)
    switch type{j}
        case 'datetime'
            md.(md.Properties.VariableNames{j})=cellstr(md.(md.Properties.VariableNames{j}));
            type{j}='char';
        case 'string'
            if iscell(md.(md.Properties.VariableNames{j}))
                md.(md.Properties.VariableNames{j}) = cellfun(@(x) convertStringsToChars(x),md.(md.Properties.VariableNames{j}),'UniformOutput',false);
            else
                md.(md.Properties.VariableNames{j}) = convertStringsToChars(md.(md.Properties.VariableNames{j}));
            end
        otherwise
            % Nothing yet
    end
end


% Make a new table with the height of data.nSample (space for unmatched
% samples)
warning off
metadata=table('Size',[data.nSample,width(md)],'VariableTypes',type,'VariableNames',md.Properties.VariableNames);
warning on

% Check for non-unique identifiers (these will make trouble during matching)
if numel(unique(lower(data.(datakey))))<data.nSample
    c = categorical(data.(datakey));
    cats=categories(c);
    counts=countcats(c);
    nonunique=cats(counts>1);
    message=['Non-unique identifiers: '];
    for j=1:numel(nonunique)
        message = [message ['\n ',num2str(j),': ',nonunique{j}]];
    end

    message = [message ['\n lower-case filenames in supplied drEEM dataset structure are not unique. Cannot perform meaningful matching.']];
    error(sprintf(message))
end
% Check for non-unique identifiers no2  (these will make trouble during matching)
if numel(unique(lower(md.(metadatakey))))<height(md)
    c = categorical(md.(metadatakey));
    cats=categories(c);
    counts=countcats(c);
    nonunique=cats(counts>1);
    message='Non-unique identifiers: ';
    for j=1:numel(nonunique)
        message = [message ['\n ',num2str(j),': ',nonunique{j}]];
    end
    if numel(nonunique)==0
        % Do nothing (table had more entries than needed)
    else
        message = [message ['\n lower-case filenames in specified metadata table are not unique. Cannot perform meaningful matching.']];
        error(sprintf(message))
    end
end

% Make sure key and lock are the same class
a=lower(data.(datakey));
b=lower(md.(metadatakey));
ca=class(a);
cb=class(b);

if matches(ca,'char')
    a=cellstr(a);ca=class(a);
end
if matches(cb,'char')
    b=cellstr(b);cb=class(b);
end

if not(matches(ca,cb))
    error(['data.',datakey,' and metadata.',metadatakey,'are not of the same class. Please convert one or both and try again.'])
end
    


% Find the intersect between the key variables
[~,ia,ib] = intersect(a,b,'stable');


% Fill the emtpy table with matches from the comparison (leaves unmatched
% empty)
metadata(ia,:) = md(ib,:);

% Fill the rest of numeric data with missing data (ensure proper missing values)
for j = 1:width(metadata)
    if matches(type{j},'double')
        metadata.(metadata.Properties.VariableNames{j})(setdiff(1:data.nSample,ia))=missing;
    elseif matches(type{j},'char')
        try
            metadata.(metadata.Properties.VariableNames{j})(setdiff(1:data.nSample,ia))='missing';
        catch
            metadata.(metadata.Properties.VariableNames{j})(setdiff(1:data.nSample,ia))={'missing'};
        end
    end
end


% Make a new structure with all the metadata fields
dataout=data;
vars = metadata.Properties.VariableNames;
for j=1:numel(vars)
    dataout.(['md_',vars{j}])=metadata.(vars{j});
end

% Support for metadata table (forward compatibility, not yet enabled due to
% subdataset).
% if not(isfield(dataout,'metadata'))
%     dataout.metadata=metadata;
% else
%     C=intersect(dataout.metadata.Properties.VariableNames,metadata.Properties.VariableNames);
%     if not(isempty(C))
%         for j=1:numel(C)
%             metadata.(C{j})=[];
%         end
%     end
%     dataout.metadata=[dataout.metadata metadata];
% end

end
