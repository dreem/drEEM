function [varargout]=specsse(data,n)
%
% <strong>Syntax</strong>
%   <strong>specsse</strong>(data,n)
%
% <a href="matlab: doc specsse">help for specsse</a> <- click on the link

% Plot SSE (sum of squared error residuals) and retreive in all
% dimensions for one or more models, to help determine the appropriate
% number of PARAFAC components. A small decrease in SSE indicates that the
% last component added might be unnesscessary or detrimental.
%
%INPUT: allsse(data,n)
% data: A data structure containing PARAFAC models with the same number(s)
%       of components as indicated in the range of n
%    n: (optional) Number(s) of components in models to be plotted
%
%EXAMPLES:
%     allsse(Test2,3)
%     allsse(Test2,3:6)
%
% Notice:
% This mfile is part of the drEEM toolbox. Please cite the toolbox
% as follows:
%
% allsse.m: Copyright (C) 2019 Kate Murphy & Urban J Wuensch
% Chalmers University of Technology
% Sven Hultins Gata 6
% 41296 Gothenburg, Sweden
% $ Version 0.3.0 $ July 2018      $ Update to include sample SSE and output of SSE
% $ Version 0.2.0 $ March 2017     $ Update to support multiple component models
% $ Version 0.1.0 $ September 2013 $ First Release
if nargin==0
    help specsse
    return
end

if ~exist('n','var')
    i=1;
    for k=1:50
        if isfield(data,['Model',num2str(k)])
            n(i)=k;
            i=i+1;
        end
    end
end

emptymat=cell([length(n) 1]);
E_ex=emptymat;E_em=emptymat;
M=emptymat;E=emptymat;
for i=n
    Model=['Model' int2str(i)];
    if isfield(data,Model)
        M{i}=nmodel(data.(Model));
        E{i}=data.X-M{i};
        intE_em = squeeze(nansum(nansum(E{i}.^2,1),2)); % misssum(squeeze(misssum((permute(E{i},[2 1 3])).^2)));
        intE_ex = squeeze(nansum(nansum(E{i}.^2,1),3));%misssum(squeeze(misssum((permute(E{i},[3 1 2])).^2)));
        intE_sample = squeeze(nansum(nansum(E{i}.^2,2),3));
        E_ex{i} = intE_em;
        E_em{i} = intE_ex;
        E_sample{i} = intE_sample;
        err{i}=[E_sample(i) E_em(i) E_ex(i)];
    end
end
if nargout==1
    varargout{1}=err;
end
leg=[num2str((n)') repmat(' comp.',[length(n) 1])];
lincol=lines(length(n));

h=dreemfig;
subplot(3,1,1)
for i=1:length(n)
    plot(1:data.nSample,E_sample{n(i)},'color',lincol(i,:),'DisplayName','sample'),hold on
end
axis tight
xlabel('Sample index')

ylabel('Sum of Squared Error')

subplot(3,1,2)
for i=1:length(n)
    plot(data.Ex,E_ex{n(i)},'color',lincol(i,:),'DisplayName','excitation'),hold on
end
axis tight
xlabel('Ex. (nm)')
ylabel('Sum of Squared Error')

subplot(3,1,3),
for i=1:length(n)
    plot(data.Em,E_em{n(i)},'color',lincol(i,:),'DisplayName','emission'),hold on
end
legend(legh(length(n),lines(length(n))),leg,'location','best')
axis tight
xlabel('Em. (nm)')
ylabel('Sum of Squared Error')
set(h,'name',['Sum of Squared Error: ' num2str(n) ' components'])
dreemfig(h);

dcm_obj = datacursormode(gcf);
set(dcm_obj,'UpdateFcn',{@parse4datacursor,data});
datacursormode on

end

function [ h ] = legh( numPlots,col )
h = gobjects(numPlots, 1);
for n=1:numPlots
    hold on;
    h(n) = line(NaN,NaN,'Marker','o','MarkerSize',5,'MarkerFaceColor',col(n,:),'MarkerEdgeColor','none','LineStyle','none');
end
end