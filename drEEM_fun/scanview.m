function scanview(DS,varargin)
%
% <strong>Syntax</strong>
%   <strong>scanview</strong>(data,Name,Value)
%
% <a href="matlab: doc scanview">help for scanview</a> <- click on the link

% Plot 2D emission scans for selected Excitation wavelengts for selected samples
%   
% USEAGE:
%           [] = scanview(DS,Name,value)
%
% INPUTS
%            DS:                drEEM-format dataset
%            (optional):        Name,value: Parameter name followed by option
%            eem:               (char): Name of variable in DS that should be plotted.
%                                      Default: 'X'
%                                      Special: ModelX     -> Modeled data (X-component model)
%                                               ModelXresi -> Residuals of X-component model
%            title:             (cell) character cells providing title above EEM.
%                                      Default: DS.filelist
%            samples:           (numeric) Selection of samples for plotting
%                                      Default: all samples in the dataset
%            ex:                (numeric) Excitation range selected for plotting. Eg. 250:350
%                               Note: Your selection does not have to be exact, the closest 
%                                   match will be identified!
%                               automatically.
%                                      Default: all.
%
%             
% Examples:
%       	1. scanview(EEMcor)
%           2. scanview(EEMcor,'ex',1:5:EEMcor.nEx,'samples',[1 5 10])
%
% Notice:
% This mfile is part of the drEEM toolbox. Please cite the toolbox
% as follows:
%
% Murphy K.R., Stedmon C.A., Graeber D. and R. Bro, Fluorescence
%     spectroscopy and multi-way techniques. PARAFAC, Anal. Methods, 2013, 
%     DOI:10.1039/c3ay41160e. 
%
% scanview: Copyright (C) 2019 Urban J. Wuensch
% Chalmers University of Technology
% Sven Hultins Gata 6
% 41296 Gothenburg
% Sweden
% wuensch@chalmers.se
% $ Version 0.1.0 $ March 2019 $ First Release

if nargin==0
    help scanview
    return
end
% Welcome messages
disp(' ')
disp(' ')
disp('scanview.m')
disp('----------')
disp('Plot Emission spectra for different excitation spectra')
disp('To proceed to the next sample, click ''Next'' or simply hit the spacebar')

disp('Close plot to cancel...')
disp('----------')

%% Input Parser
params = inputParser;

params.addParameter('eem', 'X', @ischar);
params.addParameter('samples', 1:DS.nSample, @isnumeric);
params.addParameter('ex', 1:DS.nEx, @isnumeric);
params.addParameter('title', 'filelist', @ischar);

params.parse(varargin{:});

Xname = params.Results.eem;
samples   = params.Results.samples;
inc   = params.Results.ex;
titlefield  = params.Results.title;

if numel(inc)~=numel(DS.Ex)
   inc=[inc(1) inc(end)];
   [idx1,dista] = mindist( DS.Ex,inc(1));
   if dista>50
       warning('Distance from minimum DS.Ex and your input in ''ex'' appears rather large. Input to ''ex'' is expected as wavelength')
   end
   [idx2,dista] = mindist( DS.Ex,inc(2));
   if dista>50
       warning('Distance from maximum DS.Ex and your input in ''ex'' appears rather large. Input to ''ex'' is expected as wavelength')
   end
   inc=(idx1:idx2);
end
if numel(inc)>50
    warning('MATLAB currently only supports 50 legend entries. Entries will be evenly distributed, but not every line will have a legend entry')
end

% EEM extraction
if ~any(strfind(Xname,'Model'))&&~any(strfind(Xname,'resi'))
    X=DS.(Xname); % Field extraction
    X=X(samples,:,:); %Sample selection if no special cases are selected
elseif any(strfind(Xname,'Model'))&&~any(strfind(Xname,'resi')) % In case X is model, modeled data will be caluclated
    X=DS.(Xname(1:6)); % Field extraction
    X=nmodel(X);
    X=X(samples,:,:);
elseif any(strfind(Xname,'Model'))&&any(strfind(Xname,'resi'))  % In case X is model and residuals are requested
    X=DS.(Xname(1:6)); % Field extraction
    X=nmodel(X);
    X=X(samples,:,:);
    X=DS.X(samples,:,:)-X;
end



try
    filelist=DS.(titlefield);
    filelist=filelist(samples,1);
catch
    warning('Could not field ''filelist'' or user-specified field given as option ''title''.')
    filelist=num2str(1:DS.nSample);
end


figHandles = get(0,'Children');
for n=1:numel(figHandles);if strfind(figHandles(n).Name,'scanview');close(figHandles(n));end;end
fig1=dreemfig;
set(fig1, 'units','normalized','outerposition',[0.3 0.2 .4 .5],'InvertHardcopy','off');
set(fig1,'Name','scanview')
ax=axes('pos',[0.1300    0.1924    0.7750    0.7326]);
huic(1)=uicontrol('Style', 'pushbutton','String','Next',...
    'Units','normalized','Position', [0.77 0.05 0.15 0.05]);
huic(2)=uicontrol('Style', 'pushbutton','String','Previous',...
    'Units','normalized','Position', [0.605,0.05,0.15,0.05]);

colors=colmaker(numel(inc));
sample_i=1;
while sample_i<=numel(samples)
    if ~ishandle(fig1); return; end % Ends function when plot is closed by user
    try
       %% uicontrol object
       set(huic(1),'Callback',{@nextsample,DS,sample_i,ax});
       set(huic(2),'Callback',{@prevsample,DS,sample_i,ax});
       plotscans([],[],DS,X,sample_i,filelist,samples,ax,huic,inc,colors)
       %wait for uiinput on fig1 
       uicontrol(huic)
       uiwait(gcf)
       if ~ishandle(fig1); return; end % Ends function when plot is closed by user
       counter=str2double(ax(1).Children(end).DisplayName); % Callback functions cannot return values. User decision stored in DisplayName of EEM.
       if counter==-1&&sample_i==1
           warning('I'' affraid you can''t plot the zero''th EEM. Try ''Next sample''.')
       else
           sample_i=sample_i+counter;
       end

   catch ME
        disp(' ');rethrow(ME); % Repeats last error message
   end
   % In case the last sample is reached, this will just tell the user that the end was reached.
   if 1i==DS.nSample; close all;end
end
disp(' ')
disp('Done.')
end

function plotscans(~,~,DS,X,i,filelist,samples,ax,huic,inc,colors)
       hold off
       mat=squeeze(X(i,:,:));
       cnt=1;
       for n=inc
           h(n)=plot(ax,DS.Em,mat(:,n),'LineWidth',1.7,'Color',colors(cnt,:),'Marker','.','MarkerSize',10,...
               'DisplayName',['Ex = ',num2str(DS.Ex(n))]);
           hold all
           cnt=cnt+1;
       end
       if isempty(findobj(gcf,'Tag','legend'))
           if numel(h)>50
               set(h(1:ceil(numel(h)/50):end),'HandleVisibility','off')
           end
           if numel(h)<15
               legend(ax,'location','eastoutside');
           else
               try
               legend(ax,'location','eastoutside','NumColumns',3)
               catch
                   if ~strcmp(lastwarn,'Too many legend entries. Consider updating MATLAB to support multi-column legends!')
                        warning('Too many legend entries. Consider updating MATLAB to support multi-column legends!')
                   end
               end
           end
       end
       set(ax,'fontsize',10,'FontName','Arial')
       xlabel(ax,'Emission (nm)');ylabel(ax,'Signal intensity');
       title(ax,[num2str(i),'/',num2str(numel(samples)),': ',char(filelist(i))], 'Interpreter', 'none');
       hold off
       dreemfig(gcf);
       uicontrol(huic(1))
end


function cols= colmaker(num)

% col100=[0.647058823529412,0,0.149019607843137;0.654117647058824,0.0199215686274510,0.156039215686274;0.661176470588236,0.0398431372549020,0.163058823529412;0.668235294117647,0.0597647058823529,0.170078431372549;0.675294117647059,0.0796862745098039,0.177098039215686;0.682352941176471,0.0996078431372549,0.184117647058823;0.689411764705883,0.119529411764706,0.191137254901961;0.696470588235294,0.139450980392157,0.198156862745098;0.703529411764706,0.159372549019608,0.205176470588235;0.710588235294118,0.179294117647059,0.212196078431372;0.717647058823530,0.199215686274510,0.219215686274510;0.724705882352941,0.219137254901961,0.226235294117647;0.731764705882353,0.239058823529412,0.233254901960784;0.738823529411765,0.258980392156863,0.240274509803921;0.745882352941177,0.278901960784314,0.247294117647059;0.752941176470588,0.298823529411765,0.254313725490196;0.760000000000000,0.318745098039216,0.261333333333333;0.767058823529412,0.338666666666667,0.268352941176470;0.774117647058824,0.358588235294118,0.275372549019608;0.781176470588236,0.378509803921569,0.282392156862745;0.788235294117647,0.398431372549020,0.289411764705882;0.795294117647059,0.418352941176471,0.296431372549019;0.802352941176471,0.438274509803922,0.303450980392157;0.809411764705883,0.458196078431373,0.310470588235294;0.816470588235294,0.478117647058824,0.317490196078431;0.823529411764706,0.498039215686275,0.324509803921569;0.830588235294118,0.517960784313726,0.331529411764706;0.837647058823530,0.537882352941177,0.338549019607843;0.844705882352941,0.557803921568628,0.345568627450980;0.851764705882353,0.577725490196078,0.352588235294118;0.858823529411765,0.597647058823529,0.359607843137255;0.865882352941177,0.617568627450980,0.366627450980392;0.872941176470588,0.637490196078431,0.373647058823529;0.880000000000000,0.657411764705883,0.380666666666667;0.887058823529412,0.677333333333333,0.387686274509804;0.894117647058824,0.697254901960784,0.394705882352941;0.901176470588235,0.717176470588235,0.401725490196078;0.908235294117647,0.737098039215686,0.408745098039216;0.915294117647059,0.757019607843137,0.415764705882353;0.922352941176471,0.776941176470588,0.422784313725490;0.929411764705882,0.796862745098039,0.429803921568627;0.936470588235294,0.816784313725490,0.436823529411765;0.943529411764706,0.836705882352941,0.443843137254902;0.950588235294118,0.856627450980392,0.450862745098039;0.957647058823530,0.876549019607843,0.457882352941176;0.964705882352941,0.896470588235294,0.464901960784314;0.971764705882353,0.916392156862745,0.471921568627451;0.978823529411765,0.936313725490196,0.478941176470588;0.985882352941177,0.956235294117647,0.485960784313725;0.992941176470588,0.976156862745098,0.492980392156863;1,0.996078431372549,0.500000000000000;0.983513405362145,0.980072028811525,0.501720688275310;0.967026810724290,0.964065626250500,0.503441376550620;0.950540216086435,0.948059223689476,0.505162064825930;0.934053621448579,0.932052821128451,0.506882753101241;0.917567026810724,0.916046418567427,0.508603441376551;0.901080432172869,0.900040016006403,0.510324129651861;0.884593837535014,0.884033613445378,0.512044817927171;0.868107242897159,0.868027210884354,0.513765506202481;0.851620648259304,0.852020808323329,0.515486194477791;0.835134053621449,0.836014405762305,0.517206882753101;0.818647458983594,0.820008003201281,0.518927571028411;0.802160864345738,0.804001600640256,0.520648259303722;0.785674269707883,0.787995198079232,0.522368947579032;0.769187675070028,0.771988795518207,0.524089635854342;0.752701080432173,0.755982392957183,0.525810324129652;0.736214485794318,0.739975990396159,0.527531012404962;0.719727891156463,0.723969587835134,0.529251700680272;0.703241296518608,0.707963185274110,0.530972388955582;0.686754701880752,0.691956782713085,0.532693077230892;0.670268107242897,0.675950380152061,0.534413765506203;0.653781512605042,0.659943977591037,0.536134453781513;0.637294917967187,0.643937575030012,0.537855142056823;0.620808323329332,0.627931172468988,0.539575830332133;0.604321728691477,0.611924769907963,0.541296518607443;0.587835134053621,0.595918367346939,0.543017206882753;0.571348539415766,0.579911964785914,0.544737895158063;0.554861944777911,0.563905562224890,0.546458583433373;0.538375350140056,0.547899159663866,0.548179271708683;0.521888755502201,0.531892757102841,0.549899959983994;0.505402160864346,0.515886354541817,0.551620648259304;0.488915566226491,0.499879951980792,0.553341336534614;0.472428971588635,0.483873549419768,0.555062024809924;0.455942376950780,0.467867146858744,0.556782713085234;0.439455782312925,0.451860744297719,0.558503401360544;0.422969187675070,0.435854341736695,0.560224089635854;0.406482593037215,0.419847939175670,0.561944777911164;0.389995998399360,0.403841536614646,0.563665466186475;0.373509403761505,0.387835134053621,0.565386154461785;0.357022809123650,0.371828731492597,0.567106842737095;0.340536214485794,0.355822328931573,0.568827531012405;0.324049619847939,0.339815926370548,0.570548219287715;0.307563025210084,0.323809523809524,0.572268907563025;0.291076430572229,0.307803121248499,0.573989595838335;0.274589835934374,0.291796718687475,0.575710284113645;0.258103241296519,0.275790316126451,0.577430972388956;0.241616646658663,0.259783913565426,0.579151660664266;0.225130052020808,0.243777511004402,0.580872348939576;0.208643457382953,0.227771108443377,0.582593037214886;0.192156862745098,0.211764705882353,0.584313725490196];
% cols=col100(1:floor(100/num):100,:);
try
    cols=flipud(parula(num));
catch
    cols=flipud(jet(num));
end
end


function nextsample(~,~,~,~,ax)
ax.Children(end).DisplayName='1';
uiresume
end

function prevsample(~,~,~,~,ax)
ax.Children(end).DisplayName='-1';
uiresume
end

function [idx,distance] = mindist( vec,value)
[distance,idx]=min(abs(vec-value));
end