function dreem
%% <strong>drEEM toolbox v.0.6.4</strong>
%
% <strong>Contents of the toolbox (function list):</strong>
%
% <strong>1. Getting started</strong>
%    dreeminstall                   Install the toolbox
%    getdreem                       Download the latest stable release
%    lookforconflicts               Check for conflicting functions
%
% <strong>2. Data import / export</strong>
%    checkdataset                   Validate the format of a dataset
%    readineems                     Load EEMs and assemble into a 3-way dataset
%    modelexport                    Calculate Fmax and export models to spreadsheet (Platform-independent)
%    modelout                       Calculate Fmax and export models to Excel (Windows)
%    openfluor                      Export spectra to OpenFluor format (www.openfluor.org)
%    openfluormatches               Plot match results from OpenFluor (www.openfluor.org).
%    readinscans                    Load Scans and assemble into a 2-way dataset
%    readlogfile                    Reads in a log file (*.csv)
%    associatemetadata              Read in a table containing metadata and associate it with a dataset.
%
% <strong>3. Spectral correction</strong>
%    ABAife                         Inner Filter Effect correction (absorbance method in 1 cm cell)
%    CDAife                         Inner Filter Effect correction (controlled dilution method)
%    fdomcorrect                    Apply spectral and/or inner filter corrections to EEMs
%    ramanintegrationrange          Empirical calculator of optimal Raman peak boundaries
%    RamanAreaI                     Calculate Raman areas in emission scans
%    undilute                       Adjust fluorescence intensities to account for dilution
%
% <strong>4. Data manipulation</strong>
%    alignds                        Align datasets using a csv sample log
%    assembledataset                Assemble EEMs and metadata into a dataset
%    classinfo                      Summarise metadata associated with a dataset
%    handlescatter                  See smootheem, but with new interpolation method
%    matchsamples                   Match related samples in different datasets
%    metadata                       List metadata for a dataset or its splits
%    smootheem                      Excise or smooth Rayleigh and Raman scatter
%    splitds                        Sort, split and combine samples into new datasets
%    subdataset                     Remove samples/wavelengths from a dataset
%    zap                            Remove parts of EEMs from selected samples
%
% <strong>5. PARAFAC</strong>
%    describecomp                   Describe components in terms of Ex and Em maxima
%    loadingsandleverages           Display PARAFAC loadings and leverages
%    normeem                        Normalise EEM dataset to reduce collinearity
%    nwayparafac                    Multiway PARAFAC model (from nway toolbox)
%    outliertest                    Fast PARAFAC to identify unusual samples or wavelengths
%    randinitanal                   PARAFAC with random initialisation and iteration
%    relcomporder                   Match up PARAFAC components in various split models
%    scores2fmax                    Obtain Fmax from model scores
%    specsse                        Plot sum of squared error residuals in all 3 dataset modes
%    splitanalysis                  Generate PARAFAC models nested in dataset splits
%    splitvalidation                Validate PARAFAC models 
%
% <strong>6. CDOM and FDOM indicies</strong>
%    pickpeaks                      Extract FDOM peaks and fluorescence indices
%    slopefit                       Calculate and visualize CDOM absorbance slopes
%
% <strong>7. Plotting</strong>
%    compcorrplot                   Plot scores for PARAFAC components against each other
%    comparespectra                 PARAFAC spectra in overlaid plots
%    compare2models                 Compare measured, modeled and residual EEMs for two models
%    coreandvar                     Inspect core consistency and %EV against  number of components
%    diffeem                        Calculate & visualize the difference between a reference EEM and other samples.
%    eemview                        Customised EEM contour plots
%    eemreview                      EEM contour plots and emission + excitation scans
%    errorsandleverages             Plot error vs. leverage of samples to identify issues
%    fingerprint                    PARAFAC spectra in contour plots
%    loadingsandleverages           Display PARAFAC loadings and leverages
%    outliertest                    Visualize models calculated with outliertest or randinitanal
%    scanview                       EEMs as superimposed fluorescence emission scans
%    specsse                        Plot sum of squared error residuals in all 3 dataset modes
%    spectralloadings               PARAFAC spectra in individual plots
%    spectralvariance               Explore the variance in CDOM absorbance and FDOM EEMs
% 
% <strong>Access the documentation: </strong>
% <a href="matlab: doc dreem">help for dreem</a> <- click on the link


end
