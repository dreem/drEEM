function doc(functionname)
% drEEM-specific function for displaying custom formatted documentation
% (c) Urban Wuensch, 2019
%% Function init
% This bit is for cleanup
currentdirectory = pwd;
c = onCleanup(@() cd(currentdirectory));

rtpath=matlabroot;
doclocs = which('doc','-all');
restoredoc_i=contains(doclocs,rtpath);

if numel(doclocs)>2
%     wanring(sprintf(['\n\n   Multiple versions of ''doc'' found. drEEM has it''s own function with this name.\n' ...
%         '   Under the current conditions, doc.m would not stop running. If you are unsure what this means:\n\n' ... 
%         '    1. Please <strong>delete your copy of drEEM from your hard disk.</strong> \n' ... 
%         '    2. Download the latest verison from dreem.openfluor.org \n' ... 
%         '    3. Install drEEM again.\n']))
end
% Database of published function docs
try functionname=lower(functionname);end %#ok<TRYNC>
helpdatabase = {
    'dreem',            'doc_toolbox_product_page.html'
    'getdreem',         'f_getdreem.html'
    'dreeminstall',     'f_dreeminstall.html'
    'lookforconflicts', 'f_lookforconflicts.html'
    'readineems',       'f_readineems.html'
    'readinscans',      'f_readinscans.html'
    'modelout',         'f_modelout.html'
    'openfluor',        'f_openfluor.html'
    'readlogfile',      'f_readlogfile.html'
    'assembledataset',  'f_assembledataset.html'
    'metadata',         'f_metadata.html'
    'classinfo',        'f_classinfo.html'
    'subdataset',       'f_subdataset.html'
    'randinitanal',     'f_randinitanal.html'
    'splitanalysis',    'f_splitanalysis.html'
    'splitvalidation',  'f_splitvalidation.html'
    'specsse',          'f_specsse.html'
    'loadingsandleverages','f_loadingsandleverages.html'
    'outliertest',      'f_outliertest.html'
    'smootheem',        'f_smootheem.html'
    'splitds',          'f_splitds.html'
    'zap',              'f_zap.html'
    'matchsamples',     'f_matchsamples.html'
    'alignds',          'f_alignds.html'
    'eemview',          'f_eemview.html'
    'eemreview',        'f_eemreview.html'
    'nwayparafac',      'f_nwayparafac.html'
    'describecomp',     'f_describecomp.html'
    'normeem',          'f_normeem.html'
    'scores2fmax',      'f_scores2fmax.html'
    'diffeem',          'f_diffeem.html'
    'spectralvariance', 'f_spectralvariance.html'
    'spectralloadings', 'f_spectralloadings.html'
    'fingerprint',      'f_fingerprint.html'
    'coreandvar',       'f_coreandvar.html'
    'errorsandleverages','f_errorsandleverages.html'
    'compcorrplot',     'f_compcorrplot.html'
    'compare2models',   'f_compare2models.html'
    'comparespectra',   'f_comparespectra.html'
    'pickpeaks',        'f_pickpeaks.html'
    'slopefit',         'f_slopefit.html'
    'ABAife',           'f_ABAife.html'
    'CDAife',           'f_CDAife.html'
    'fdomcorrect',      'f_fdomcorrect.html'
    'ramanintegrationrange','f_ramanintegrationrange.html'
    'undilute',         'f_undilute.html'
    'checkdataset',     'f_checkdataset.html'
    'scanview',         'f_scanview.html'
    'relcomporder',     'f_relcomporder.html'
    'handlescatter',    'f_handlescatter.html'
    'openfluormatches', 'f_openfluormatches.html'
    'modelexport',      'f_modelexport.html'
    'RamanAreaI',       'f_RamanAreaI.html'
    'associatemetadata' 'f_associatemetadata.html'
    };


%% Catch users that just want to see the start page of doc 
if nargin<1
    cd(fileparts(doclocs{restoredoc_i}));
    doc
    return
end

%% Search for existing documentation based on entries in "functiondirectory"
for i = 1:size(helpdatabase,1)
    if strcmpi(functionname, helpdatabase{i,1})
        web(helpdatabase{i,2}, '-notoolbar','-new');
        return
    end
end

%% If nothing was found, just display the default MATLAB output
cd(fileparts(doclocs{restoredoc_i}));
doc(functionname);
end